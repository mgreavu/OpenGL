#include "stdafx.h"
#include "SGRendererGL.h"
#include "common/context/GLContext.h"
#include "common/camera/Projector.h"
#include "common/model3d/Model3d.h"
#include "common/model3d/Material.h"
#include "common/model3d/Texture.h"
#include "common/SceneNode.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>


SGRendererGL::SGRendererGL()
    : m_pGLCtx( NULL)
    , m_flags( RenderFlags::FLAGS_NONE )
{
}

SGRendererGL::~SGRendererGL()
{
}

bool SGRendererGL::Init( GLContext11* pGLCtx, const shared_ptr<IProjector>& proj )
{
    m_pGLCtx = pGLCtx;
    m_pProj = proj;

    bool bOk = true;
    bOk = bOk && m_pGLCtx->Select();
    bOk = bOk && m_program.Init();

    if ( bOk )
    {
        glEnable(GL_DEPTH_TEST);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        glActiveTexture(GL_TEXTURE0);   // we use only one texture unit
        m_program.Enable();
        m_program.SetTextureUnit( 0 );  // first texture unit

        m_dirLight.m_color = glm::vec3(1.0f, 1.0f, 1.0f);
        m_dirLight.m_ambientIntensity = 0.25f;
        m_dirLight.m_direction = glm::vec3(1.0f, -1.0f, -1.0f);
        m_dirLight.m_diffuseIntensity = 0.75f;

        glEnableVertexAttribArray(0);
    }
    return bOk;
}

void SGRendererGL::CleanUp()
{
    if (m_pGLCtx != NULL)
    {
        if (::wglGetCurrentContext() != NULL)
        {
            glDisableVertexAttribArray(0);
            RemoveScene();
        }
    }
}

void SGRendererGL::Resize( int cx, int cy )
{
    m_pProj->SetViewport( glm::i32vec2(cx, cy) );
    glViewport(0, 0, cx, cy);
}

void SGRendererGL::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if ( m_pScene )
    {
        m_program.Enable();
        RenderScene( m_pScene.get() );
    }

    m_pGLCtx->SwapBuffers();
}

void SGRendererGL::RenderScene( SceneNode* pScene )
{
    const Model3d* pModel = pScene->GetModel();
    if ( pModel )
    {
        glm::mat4 matWorld = pScene->GetWorldTransform() * glm::scale( glm::mat4(), pScene->GetScale() );
        glm::mat4 matMVP = m_pProj->GetCombinedMVPMatrix() * matWorld;

        m_program.SetMVP( matMVP );
        m_program.SetWorldMatrix( matWorld );

        m_ffCuller.Load( matMVP );

        glEnableVertexAttribArray(2);

        for ( Model3d::ConstIter itMesh = pModel->GetMeshesBegin();
            itMesh != pModel->GetMeshesEnd(); ++itMesh )
        {
            // frustum culling
            if ( m_ffCuller.RectPrismCulled( (*itMesh).m_bbox ) == FFC_CULLED )
            {
//              dbg("Culled submesh in '%s'\r\n", pModel->GetName().c_str());
                continue;
            }

            glBindBuffer(GL_ARRAY_BUFFER, (*itMesh).m_VB);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(sizeof(glm::vec3) + sizeof(glm::vec2)));

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*itMesh).m_IB);

            const Material* pMaterial = (*itMesh).m_pMaterial.get();
            ASSERT( pMaterial );

            if ( (m_flags & RenderFlags::FLAG_ENBLE_TEXTURES) &&  pMaterial->m_pTexture )
            {
                m_program.SetHasTexture(true);
                glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)sizeof(glm::vec3));
                glEnableVertexAttribArray(1);

                glBindTexture( pMaterial->m_pTexture->GetTexTarget(), pMaterial->m_pTexture->GetTexObj() );
            }
            else
            {
                m_program.SetHasTexture(false);
            }
            m_dirLight.m_color = glm::vec3(pMaterial->m_Kd[0], pMaterial->m_Kd[1], pMaterial->m_Kd[2]);
            m_program.SetDirectionalLight(m_dirLight);

            glDrawElements(GL_TRIANGLES, (*itMesh).m_Indices.size(), GL_UNSIGNED_INT, 0);

            if ( (m_flags & RenderFlags::FLAG_ENBLE_TEXTURES) &&  pMaterial->m_pTexture )
            {
                glDisableVertexAttribArray(1);
            }
        }

        glDisableVertexAttribArray(2);
    }

    for ( SceneNode::ConstIter it = pScene->GetChildrenBegin();
        it != pScene->GetChildrenEnd(); ++it )
    {
        RenderScene( *it );
    }
}

void SGRendererGL::SetScene( const shared_ptr<SceneNode>& pScene )
{
    m_pScene = pScene;

    if ( m_pScene )
    {
        PrepareScene( m_pScene.get() );
    }
}

void SGRendererGL::RemoveScene()
{
    if ( m_pScene )
    {
        RemoveScene( m_pScene.get() );
        m_pScene.reset();
    }
}

void SGRendererGL::PrepareScene( SceneNode* pScene )
{
    Model3d* pModel = pScene->GetModel();
    if ( pModel )
    {
        for ( Model3d::Iter itMesh = pModel->GetMeshesBegin();
            itMesh != pModel->GetMeshesEnd(); ++itMesh )
        {
            glGenBuffers(1, &(*itMesh).m_VB);
            glBindBuffer(GL_ARRAY_BUFFER, (*itMesh).m_VB);
            glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * (*itMesh).m_Vertices.size(), &(*itMesh).m_Vertices[0], GL_STATIC_DRAW);

            glGenBuffers(1, &(*itMesh).m_IB);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*itMesh).m_IB);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * (*itMesh).m_Indices.size(), &(*itMesh).m_Indices[0], GL_STATIC_DRAW);

            ASSERT( (*itMesh).m_pMaterial );
            Texture* pTex = (*itMesh).m_pMaterial->m_pTexture.get();
            if ( pTex && !pTex->IsReady() )
            {
                GLuint texObj;
                glGenTextures( 1, &texObj );
                pTex->SetTexObj( texObj );

                GLenum texTarget = pTex->GetTexTarget();

                glBindTexture(texTarget, texObj);
                glTexParameteri(texTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(texTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                unsigned int cols, rows;
                const void* pData = pTex->GetData(cols, rows);
                glTexImage2D(texTarget, 0, GL_RGBA, cols, rows, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData);

                pTex->SetReady(true);

                dbg("PrepareScene() --> texTarget = %d, texObj = %d\r\n", pTex->GetTexTarget(), pTex->GetTexObj());
            }
        }
    }

    for ( SceneNode::ConstIter it = pScene->GetChildrenBegin();
        it != pScene->GetChildrenEnd(); ++it )
    {
        PrepareScene(*it);
    }
}

void SGRendererGL::RemoveScene( SceneNode* pScene )
{
    const Model3d* pModel = pScene->GetModel();
    if ( pModel )
    {
        for ( Model3d::ConstIter itMesh = pModel->GetMeshesBegin();
            itMesh != pModel->GetMeshesEnd(); ++itMesh )
        {
            Texture* pTex = (*itMesh).m_pMaterial->m_pTexture.get();
            if ( pTex && pTex->IsReady() )
            {
                GLuint texObj = pTex->GetTexObj();
                glDeleteTextures(1, &texObj);
//              dbg("RemoveScene() --> texObj = %d, glError() = %d\r\n", texObj, glGetError());

                pTex->SetReady(false);
            }

            glDeleteBuffers(1, &(*itMesh).m_IB);
            glDeleteBuffers(1, &(*itMesh).m_VB);
//          dbg("RemoveScene() --> glError() = %d\r\n", glGetError());
        }
    }

    for ( SceneNode::ConstIter it = pScene->GetChildrenBegin();
        it != pScene->GetChildrenEnd(); ++it )
    {
        RemoveScene(*it);
    }
}

void SGRendererGL::SetFlags( int flags )
{
    if ( (flags & RenderFlags::FLAG_DRAW_WIREFRAME) != (m_flags & RenderFlags::FLAG_DRAW_WIREFRAME) )
    {
        (flags & RenderFlags::FLAG_DRAW_WIREFRAME) ? m_pGLCtx->SetPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
            : m_pGLCtx->SetPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }
    m_flags = flags;
}

void SGRendererGL::SetCullMode( CullMode mode )
{
    switch( mode )
    {
    case CULL_NONE:
        {
            glDisable(GL_CULL_FACE);
            break;
        }
    case CULL_BACK:
        {
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);
            break;
        }
    case CULL_FRONT:
        {
            glCullFace(GL_FRONT);
            glEnable(GL_CULL_FACE);
            break;
        }
    }
}
