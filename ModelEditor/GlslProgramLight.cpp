#include "stdafx.h"
#include "GlslProgramLight.h"
#include <glm/gtc/type_ptr.hpp>

GlslProgramLight::GlslProgramLight()
{
}

bool GlslProgramLight::Init()
{
    if (!GlslProgram::Init())
    {
        return false;
    }

    if (!AddShader(GL_VERTEX_SHADER, "urban.vs"))
    {
        return false;
    }

    if (!AddShader(GL_FRAGMENT_SHADER, "urban.fs"))
    {
        return false;
    }

    if (!Finalize())
    {
        return false;
    }

    m_MVPLocation = GetUniformLocation("gWVP");
    m_WorldMatrixLocation = GetUniformLocation("gWorld");
    m_samplerLocation = GetUniformLocation("gSampler");
    m_hasTextureLocation = GetUniformLocation("gHasTexture");

    m_dirLightColorLocation = GetUniformLocation("gDirectionalLight.Color");
    m_dirLightAmbientIntensityLocation = GetUniformLocation("gDirectionalLight.AmbientIntensity");
    m_dirLightDirectionLocation = GetUniformLocation("gDirectionalLight.Direction");
    m_dirLightDiffuseIntensityLocation = GetUniformLocation("gDirectionalLight.DiffuseIntensity");

    if (m_MVPLocation == INVALID_UNIFORM_LOCATION ||
        m_WorldMatrixLocation == INVALID_UNIFORM_LOCATION ||
        m_samplerLocation == INVALID_UNIFORM_LOCATION ||
        m_hasTextureLocation == INVALID_UNIFORM_LOCATION ||
        m_dirLightAmbientIntensityLocation == INVALID_UNIFORM_LOCATION ||
        m_dirLightColorLocation == INVALID_UNIFORM_LOCATION ||
        m_dirLightDirectionLocation == INVALID_UNIFORM_LOCATION ||
        m_dirLightDiffuseIntensityLocation == INVALID_UNIFORM_LOCATION)
    {
        return false;
    }

    return true;
}

void GlslProgramLight::SetMVP(const glm::mat4& MVP)
{
    glUniformMatrix4fv(m_MVPLocation, 1, GL_FALSE, glm::value_ptr(MVP));
}

void GlslProgramLight::SetWorldMatrix(const glm::mat4& World)
{
    glUniformMatrix4fv(m_WorldMatrixLocation, 1, GL_FALSE, glm::value_ptr(World));
}

void GlslProgramLight::SetHasTexture(bool hasTexture)
{
    glUniform1i(m_hasTextureLocation, hasTexture);
}

void GlslProgramLight::SetTextureUnit(unsigned int TextureUnit)
{
    glUniform1i(m_samplerLocation, TextureUnit);
}

void GlslProgramLight::SetDirectionalLight(const DirectionalLight& Light)
{
    glUniform3f(m_dirLightColorLocation, Light.m_color.x, Light.m_color.y, Light.m_color.z);
    glUniform1f(m_dirLightAmbientIntensityLocation, Light.m_ambientIntensity);
    glm::vec3 direction = glm::normalize(Light.m_direction);
    glUniform3f(m_dirLightDirectionLocation, direction.x, direction.y, direction.z);
    glUniform1f(m_dirLightDiffuseIntensityLocation, Light.m_diffuseIntensity);
}
