#pragma once
#include <memory>

using std::shared_ptr;

class WindowGL;
class IProjector;
class SceneNode;
class SceneGraph;

class CModelEditorDlg : public CDialogEx
{
public:
	CModelEditorDlg(CWnd* pParent = NULL);

	enum { IDD = IDD_MODELEDITOR_DIALOG };

protected:
	HICON m_hIcon;
	CMenu m_menu;

	int						m_flags;
	shared_ptr<WindowGL>	m_pWindowGL;
	shared_ptr<IProjector>  m_pProj;
	shared_ptr<SceneGraph>  m_pSceneGraph;

	virtual void DoDataExchange(CDataExchange* pDX);
	virtual void OnCancel();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnFileLoad();
	afx_msg void OnFileExit();
	DECLARE_MESSAGE_MAP()
};
