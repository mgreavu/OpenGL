#include "stdafx.h"
#include "ModelEditor.h"
#include "ModelEditorDlg.h"
#include "AboutDlg.h"
#include "afxdialogex.h"
#include "WindowGL.h"
#include "common/camera/ProjectorFPS.h"
#include "SceneGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CModelEditorDlg::CModelEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CModelEditorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CModelEditorDlg::OnCancel()
{
	dbg("CModelEditorDlg::OnCancel()\r\n");

	m_pWindowGL->Release();

	CDialogEx::OnCancel();
}

void CModelEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CModelEditorDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_COMMAND(ID_FILE_LOAD, &CModelEditorDlg::OnFileLoad)
	ON_COMMAND(ID_FILE_EXIT, &CModelEditorDlg::OnFileExit)
END_MESSAGE_MAP()


// CModelEditorDlg message handlers

BOOL CModelEditorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if ( !m_menu.LoadMenu(IDR_MENU_MAIN) )
	{
		AfxThrowResourceException();
	}
	SetMenu( &m_menu );

	MoveWindow( CRect(CPoint(0, 0), CPoint(1066, 600)), FALSE );

	m_pProj.reset( new ProjectorFPS( 60.0f, 0.2f, 400.0f ) );
	m_pProj->Update();  // virtual, avoid calling it in the constructor
	m_pProj->MoveUp(2.0f);  // elevate ourselves a bit over the origin

#ifdef _DEBUG
	m_pWindowGL.reset( new WindowGL(50) );
#else
	m_pWindowGL.reset( new WindowGL(16) ); // higher fps
#endif

	m_pWindowGL->SetProjector( m_pProj );

	CRect rcClient;
	GetClientRect(rcClient);
	bool bOk = ( m_pWindowGL->Create( NULL, NULL, WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE, rcClient, this, IDC_CANVAS ) != -1);
	if ( bOk )
	{
		m_flags = RenderFlags::FLAG_ENABLE_LIGHTS | RenderFlags::FLAG_ENBLE_TEXTURES;
		m_pWindowGL->SetFlags(m_flags);
		m_pWindowGL->SetCullMode( CullMode::CULL_BACK );

		m_pWindowGL->EnableRefresh( true );
		m_pWindowGL->SetFocus();
	}

	m_pSceneGraph.reset( new SceneGraph() );

	return TRUE;
}

void CModelEditorDlg::OnFileLoad()
{
	if (!( m_pWindowGL && ::IsWindow(m_pWindowGL->GetSafeHwnd()) ))
	{
		return;
	}

	TCHAR szFilters[]= _T("Wavefront Files (*.obj)|*.obj|3D Studio Max Files (*.3ds)|*.3ds|All Files (*.*)|*.*||");
	CFileDialog fileDlg ( TRUE, _T("3ds"), _T("*.3ds"), OFN_FILEMUSTEXIST, szFilters, this );
	if( fileDlg.DoModal () != IDOK )
	{
		return;
	}

	CString m_strPathname = fileDlg.GetPathName();
	CT2CA pszConvertedAnsiString ( m_strPathname );
	string strStd( pszConvertedAnsiString );

	if ( m_pSceneGraph->Load( strStd ) )
	{
		// SceneGraphPtr converted to SceneNodePtr
		m_pWindowGL->SetScene( m_pSceneGraph );
	}
}

void CModelEditorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

BOOL CModelEditorDlg::OnEraseBkgnd(CDC* pDC)
{
	return ( m_pWindowGL && ::IsWindow(m_pWindowGL->GetSafeHwnd()) ) ? TRUE : CDialogEx::OnEraseBkgnd( pDC );
}

void CModelEditorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

void CModelEditorDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if ( m_pWindowGL && ::IsWindow(m_pWindowGL->GetSafeHwnd()) )
	{
		CRect rcClient;
		GetClientRect(rcClient);
		m_pWindowGL->MoveWindow(rcClient);
	}
}

void CModelEditorDlg::OnFileExit()
{
	 OnCancel();
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CModelEditorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
