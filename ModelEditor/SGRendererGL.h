#pragma once
#include "common/renderers/SceneGraphRenderer.h"
#include "common/FastFrustumCulling.h"
#include "GlslProgramLight.h"

class GLContext11;
class IProjector;

class SGRendererGL : public ISceneGraphRenderer
{
public:
    SGRendererGL();
    virtual ~SGRendererGL();

    bool Init( GLContext11* pGLCtx, const shared_ptr<IProjector>& proj );
    void SetFlags( int flags );
    void SetCullMode( CullMode mode );
    void SetScene( const shared_ptr<SceneNode>& pScene );
    void Render();
    void RemoveScene();
    void CleanUp();
    void Resize( int cx, int cy );

protected:
    void PrepareScene( SceneNode* pScene );
    void RenderScene( SceneNode* pScene );
    void RemoveScene( SceneNode* pScene );

    GLContext11* m_pGLCtx;
    shared_ptr<IProjector> m_pProj;
    int m_flags;

    shared_ptr<SceneNode> m_pScene;
    FastFrustumCulling m_ffCuller;
    GlslProgramLight m_program;

    DirectionalLight m_dirLight;    // directionalLight
};

