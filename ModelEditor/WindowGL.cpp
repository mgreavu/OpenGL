#include "stdafx.h"
#include "WindowGL.h"
#include "common/context/GLContext.h"
#include "SGRendererGL.h"
#include "common/SceneNode.h"
#include "common/camera/Projector.h"


WindowGL::WindowGL( UINT interval )
    : DrawingSurface( 5745, interval, DrawingSurface::CONTEXT )
{
    dbg("WindowGL()\r\n");
}

WindowGL::~WindowGL()
{
    dbg("~WindowGL()\r\n");
}

BEGIN_MESSAGE_MAP(WindowGL, DrawingSurface)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_TIMER()
END_MESSAGE_MAP()


void WindowGL::Release()
{
	dbg("WindowGL::Release()\r\n");

	if ( m_pRenderer )
	{
		m_pRenderer->CleanUp();
		delete m_pRenderer;
		m_pRenderer = NULL;
	}
}

int WindowGL::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if ( DrawingSurface::OnCreate(lpCreateStruct) == -1 )
    {
        return -1;
    }

    ASSERT( m_ctxType == DrawingSurface::CONTEXT );
	m_pRenderer = new SGRendererGL();
    ASSERT( m_pRenderer );

    if ( !m_pRenderer->Init( m_pGLCtx, m_pProj ) )
    {
        delete m_pRenderer;
        m_pRenderer = NULL;

        m_pGLCtx->Close( m_hWnd );
        delete m_pGLCtx;
        m_pGLCtx = NULL;

        return -1;
    }

    return 0;
}

void WindowGL::SetScene( const shared_ptr<SceneNode>& pScene )
{
	m_pScene = pScene;
	m_pRenderer->SetScene( m_pScene );
}

void WindowGL::OnTimer(UINT_PTR nIDEvent)
{
    if ( nIDEvent == m_tmrId )
    {
        if ( m_pScene )
        {
            m_pScene->Update();
        }
        m_pProj->Update();
        m_pRenderer->Render();
    }

    DrawingSurface::OnTimer(nIDEvent);
}

void WindowGL::OnSize(UINT nType, int cx, int cy)
{
    DrawingSurface::OnSize(nType, cx, cy);

    m_pRenderer->Resize( cx, cy );
}

void WindowGL::SetFlags( int flags )
{
    m_pRenderer->SetFlags( flags );

    DrawingSurface::SetFlags(flags);
}

void WindowGL::SetCullMode( CullMode mode )
{
    m_pRenderer->SetCullMode( static_cast<CullMode>(mode) );
}
