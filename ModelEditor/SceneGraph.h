#pragma once
#include "common\SceneNode.h"
#include <string>

using std::string;

class SceneGraph : public SceneNode
{
public:
    SceneGraph();
    virtual ~SceneGraph();

	bool Load( const string& fileName );
};
