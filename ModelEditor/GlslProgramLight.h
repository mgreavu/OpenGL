#pragma once
#include "common/GlslProgram.h"
#include "common/lights/DirectionalLight.h"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"


class GlslProgramLight : public GlslProgram
{
public:

    GlslProgramLight();

    bool Init();

    void SetMVP(const glm::mat4& MVP);
    void SetWorldMatrix(const glm::mat4& World);
    void SetTextureUnit(unsigned int TextureUnit);
    void SetHasTexture(bool hasTexture);
    void SetDirectionalLight(const DirectionalLight& Light);

private:

    GLuint m_MVPLocation;
    GLuint m_WorldMatrixLocation;
    GLuint m_samplerLocation;
    GLuint m_hasTextureLocation;
    GLuint m_dirLightColorLocation;
    GLuint m_dirLightAmbientIntensityLocation;
    GLuint m_dirLightDirectionLocation;
    GLuint m_dirLightDiffuseIntensityLocation;
};
