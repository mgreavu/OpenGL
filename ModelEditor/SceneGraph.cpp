#include "stdafx.h"
#include "SceneGraph.h"
#include "common/BoundingBox.h"
#include "common/model3d/Model3d.h"
#include "common/model3d/ModelLoader.h"
#include <glm/gtc/matrix_transform.hpp>


SceneGraph::SceneGraph()
{
}

SceneGraph::~SceneGraph()
{
}

bool SceneGraph::Load( const string& fileName )
{
	ModelLoader loader;
	shared_ptr<Model3d> pModel3d;
	SceneNode* pNode = NULL;
	BoundingBox bbox;
	glm::vec3 center;

	float scale = 1.0f;

	////////////////////////////////////////////////////////////////////
	pModel3d = loader.Load( "SkyA", fileName );
	if ( pModel3d )
	{
		bbox = pModel3d->GetBBox();
		center = bbox.center();

		scale = 0.055f;

		pNode = new SceneNode( pModel3d );
		pNode->SetTrans( glm::vec3(6.5f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -20.0f) );
		pNode->SetRotations(90.0f, 0, 0);
		pNode->SetOrigin( center );
		pNode->SetScale( scale );

		AddChild(pNode);
	}

	return ( pModel3d != NULL );
}
