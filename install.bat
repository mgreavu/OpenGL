REM ========== Install binaries ==========
mkdir bin
copy external\msvcr110\msvcr110d.dll bin /Y
copy external\msvcr110\msvcp110d.dll bin /Y
copy external\glew-1.11.0-x86-msvcr110\bin\glew32d.dll bin /Y
copy external\assimp-x86-msvcr110\bin\assimpd.dll bin /Y
copy external\FreeImage-3.16.0-x86-msvcr110\bin\FreeImaged.dll bin /Y
mkdir bin\rel
copy external\msvcr110\msvcr110.dll bin\rel /Y
copy external\msvcr110\msvcp110.dll bin\rel /Y
copy external\glew-1.11.0-x86-msvcr110\bin\glew32.dll bin\rel /Y
copy external\assimp-x86-msvcr110\bin\assimp.dll bin\rel /Y
copy external\FreeImage-3.16.0-x86-msvcr110\bin\FreeImage.dll bin\rel /Y

REM ========== Install shaders ==========
xcopy shaders bin /E /Y
xcopy shaders bin\rel /E /Y

REM ========== Install data ==========
mkdir bin\data
xcopy data bin\data /E /Y
mkdir bin\rel\data
xcopy data bin\rel\data /E /Y
