#version 330

layout (location = 0) in vec3 Position;

uniform mat4 gMVP;
uniform vec3 gGeoCenter;

void main()
{
    gl_Position = gMVP * vec4(Position - gGeoCenter, 1.0);
}
