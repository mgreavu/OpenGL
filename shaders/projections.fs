#version 330

out vec4 FragColor;

struct DirectionalLight
{
    vec3 Color;
    float AmbientIntensity;
};

uniform DirectionalLight gDirectionalLight;

void main()
{
    FragColor = vec4(gDirectionalLight.Color, 1.0f) *
                gDirectionalLight.AmbientIntensity;
}
