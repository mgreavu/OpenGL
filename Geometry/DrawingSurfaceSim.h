#pragma once
#include "common/DrawingSurface.h"

class ISceneGraphRenderer;
class SceneNode;
class SkyBox;

class DrawingSurfaceSim : public DrawingSurface
{
public:
    DrawingSurfaceSim( UINT interval, ContextType ctxType );
    virtual ~DrawingSurfaceSim();

    void SetFlags( int flags );
    void SetCullMode( CullMode mode );
    void SetScene( const shared_ptr<SceneNode>& pScene );

    void Release();

protected:
    shared_ptr<SceneNode> m_pScene;
    ISceneGraphRenderer*  m_pRenderer;

    afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnTimer(UINT_PTR nIDEvent);

    DECLARE_MESSAGE_MAP()
};

