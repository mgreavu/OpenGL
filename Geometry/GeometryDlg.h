#pragma once
#include "afxwin.h"
#include <memory>

using std::shared_ptr;

class DrawingSurfaceSim;
class IProjector;
class SceneNode;
class SceneGraph;

class CGeometryDlg : public CDialogEx
{
public:
    CGeometryDlg(CWnd* pParent = NULL); // standard constructor
    ~CGeometryDlg();

    enum { IDD = IDD_GEOMETRY_DIALOG };

    BOOL PreTranslateMessage(MSG* pMsg);

protected:
    bool InitDS();

    virtual void DoDataExchange(CDataExchange* pDX);
    virtual BOOL OnInitDialog();
    virtual void OnCancel();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnFileLoad();
    afx_msg void OnFileOpengl11();
    afx_msg void OnFileOpengl3x();
    afx_msg void OnFileExit();
    afx_msg void OnToolsCamera();
    afx_msg void OnToolsScenegraph();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnHelpAbout();

    DECLARE_MESSAGE_MAP()

    HICON m_hIcon;
    CMenu m_menu;

    shared_ptr<DrawingSurfaceSim> m_pDrawingSurface;
    shared_ptr<IProjector>     m_pProj;
    shared_ptr<SceneGraph>     m_pSceneGraph;
    SceneNode*                 m_pTarget;
    float                      m_t;

    int m_cullMode;
    int m_flags;
};
