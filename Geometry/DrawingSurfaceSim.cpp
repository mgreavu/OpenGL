#include "stdafx.h"
#include "DrawingSurfaceSim.h"
#include "renderers/SGRendererGL.h"
#include "renderers/SGRendererGL11.h"
#include "common/SceneNode.h"
#include "common/camera/Projector.h"


DrawingSurfaceSim::DrawingSurfaceSim( UINT interval, ContextType ctxType )
    : DrawingSurface( 5744, interval, ctxType )
    , m_pRenderer( NULL )
{
    dbg("DrawingSurfaceSim()\r\n");
}

DrawingSurfaceSim::~DrawingSurfaceSim()
{
    dbg("~DrawingSurfaceSim()\r\n");
}

BEGIN_MESSAGE_MAP(DrawingSurfaceSim, DrawingSurface)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_TIMER()
END_MESSAGE_MAP()

void DrawingSurfaceSim::Release()
{
    dbg("DrawingSurfaceSim::Release()\r\n");

    if ( m_pRenderer )
    {
        m_pRenderer->CleanUp();
        delete m_pRenderer;
        m_pRenderer = NULL;
    }
}

int DrawingSurfaceSim::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if ( DrawingSurface::OnCreate(lpCreateStruct) == -1 )
    {
        return -1;
    }

    ( m_ctxType == DrawingSurface::CONTEXT_11 ) ? m_pRenderer = new SGRendererGL11() : m_pRenderer = new SGRendererGL();
    ASSERT( m_pRenderer );

    if ( !m_pRenderer->Init( m_pGLCtx, m_pProj ) )
    {
        delete m_pRenderer;
        m_pRenderer = NULL;

        m_pGLCtx->Close( m_hWnd );
        delete m_pGLCtx;
        m_pGLCtx = NULL;

        return -1;
    }

    return 0;
}

void DrawingSurfaceSim::SetScene( const shared_ptr<SceneNode>& pScene )
{
    m_pScene = pScene;
    m_pRenderer->SetScene( m_pScene );
}

void DrawingSurfaceSim::OnTimer(UINT_PTR nIDEvent)
{
    // our rudimentary game loop
    if ( nIDEvent == m_tmrId )
    {
        if ( m_pScene )
        {
            m_pScene->Update();
        }
        m_pProj->Update();
        m_pRenderer->Render();
    }

    DrawingSurface::OnTimer(nIDEvent);
}

void DrawingSurfaceSim::OnSize(UINT nType, int cx, int cy)
{
    DrawingSurface::OnSize(nType, cx, cy);

    m_pRenderer->Resize( cx, cy );
}

void DrawingSurfaceSim::SetFlags( int flags )
{
    m_pRenderer->SetFlags( flags );

    DrawingSurface::SetFlags(flags);
}

void DrawingSurfaceSim::SetCullMode( CullMode mode )
{
    m_pRenderer->SetCullMode( static_cast<CullMode>(mode) );
}
