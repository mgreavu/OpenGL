//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Geometry.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GEOMETRY_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_CAM_VECTORS                 129
#define IDD_SCENEGRAPH                  131
#define IDR_MENU1                       132
#define IDR_MENU_MAIN                   132
#define IDC_CANVAS                      1000
#define IDC_POSX                        1001
#define IDC_POSY                        1002
#define IDC_POSZ                        1003
#define IDC_ATX                         1004
#define IDC_ATY                         1005
#define IDC_ATZ                         1006
#define IDC_UPX                         1007
#define IDC_UPY                         1008
#define IDC_UPZ                         1009
#define IDC_SCENEGRAPH                  1012
#define IDC_M00                         1013
#define IDC_M10                         1014
#define IDC_M20                         1015
#define IDC_M30                         1016
#define IDC_M01                         1017
#define IDC_M11                         1018
#define IDC_M21                         1019
#define IDC_M31                         1020
#define IDC_M02                         1021
#define IDC_M12                         1022
#define IDC_M22                         1023
#define IDC_M32                         1024
#define IDC_M03                         1025
#define IDC_M13                         1026
#define IDC_M23                         1027
#define IDC_M33                         1028
#define IDC_MV00                        1029
#define IDC_MV10                        1030
#define IDC_MV20                        1031
#define IDC_MV30                        1032
#define IDC_MV01                        1033
#define IDC_MV11                        1034
#define IDC_MV21                        1035
#define IDC_MV31                        1036
#define IDC_MV02                        1041
#define IDC_MV12                        1042
#define IDC_MV22                        1043
#define IDC_MV32                        1044
#define IDC_MV03                        1045
#define IDC_MV13                        1046
#define IDC_MV23                        1047
#define IDC_MV33                        1048
#define ID_HELP_ABOUT                   32771
#define ID_FILE_EXIT                    32772
#define ID_FILE_OPENGL11                32773
#define ID_FILE_OPENGL3X                32774
#define ID_FILE_LOAD                    32775
#define ID_TOOLS_CAMERA                 32776
#define ID_TOOLS_SCENEGRAPH             32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1049
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
