#pragma once
#include "afxcmn.h"
#include <glm/mat4x4.hpp>
#include <memory>

using std::shared_ptr;

class SceneNode;

class SceneGraphDlg : public CDialogEx
{
    DECLARE_DYNAMIC(SceneGraphDlg)

public:
    SceneGraphDlg( CWnd* pParent, const glm::mat4& matView, const shared_ptr<SceneNode>& sceneGraph );
    virtual ~SceneGraphDlg();

    enum { IDD = IDD_SCENEGRAPH };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    void DDX_Text_Formatted(CDataExchange* pDX,
        int nIDC,
        float& value,
        LPCTSTR lpszOutFormat=_T("%lf"),
        LPCTSTR lpszInFormat=_T("%f"));

    virtual BOOL OnInitDialog();

    afx_msg void OnTvnSelchangedScenegraph(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    afx_msg void OnBnClickedOk();

    HTREEITEM Insert( HTREEITEM hParent, SceneNode* pNode );
    void UpdateMatrix( const SceneNode* pNode );

    DECLARE_MESSAGE_MAP()

    glm::mat4 m_matView;
    shared_ptr<SceneNode> m_SceneGraph;
    CTreeCtrl m_ctrlSceneGraph;

    float m_m00;
    float m_m01;
    float m_m02;
    float m_m03;
    float m_m10;
    float m_m11;
    float m_m12;
    float m_m13;
    float m_m20;
    float m_m21;
    float m_m22;
    float m_m23;
    float m_m30;
    float m_m31;
    float m_m32;
    float m_m33;

    float m_mv00;
    float m_mv01;
    float m_mv02;
    float m_mv03;
    float m_mv10;
    float m_mv11;
    float m_mv12;
    float m_mv13;
    float m_mv20;
    float m_mv21;
    float m_mv22;
    float m_mv23;
    float m_mv30;
    float m_mv31;
    float m_mv32;
    float m_mv33;
};
