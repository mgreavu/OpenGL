
// Geometry.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
    #error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CGeometryApp:
// See Geometry.cpp for the implementation of this class
//

class CGeometryApp : public CWinApp
{
public:
    CGeometryApp();

    virtual BOOL InitInstance();
    virtual int ExitInstance();

    DECLARE_MESSAGE_MAP()
};

extern CGeometryApp theApp;
