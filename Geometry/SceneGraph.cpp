#include "stdafx.h"
#include "SceneGraph.h"
#include "common/BoundingBox.h"
#include "common/model3d/Model3d.h"
#include "common/model3d/ModelLoader.h"
#include <glm/gtc/matrix_transform.hpp>

#define _LARGE_MODELS

SceneGraph::SceneGraph()
{
}

SceneGraph::~SceneGraph()
{
}

SceneNode* SceneGraph::GetRoot()
{
    return this;
}

template <>
SceneNode* SceneGraph::Find( SceneNode* pFrom, const string& id )
{
    if ( !pFrom )
    {
        return NULL;
    }

    const Model3d* pModel = pFrom->GetModel();
    if ( pModel && pModel->GetName() == id )
    {
        return pFrom;
    }

    SceneNode* pFound = NULL;
    for ( SceneNode::ConstIter it = pFrom->GetChildrenBegin();
        it != pFrom->GetChildrenEnd(); ++it )
    {
        if ( (pFound = Find( *it, id )) != NULL )
        {
            break;
        }
    }

    return pFound;
}

bool SceneGraph::Load()
{
    ModelLoader loader;
    shared_ptr<Model3d> pModel3d;
    SceneNode* pNode = NULL;
    BoundingBox bbox;
    glm::vec3 center;

    float scale = 1.0f;

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load( "SkyA", "d:\\Media\\3DModels\\Buildings\\CPL5\\SkyA.3DS" );
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.055f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(6.5f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -20.0f) );
        pNode->SetRotations(90.0f, 0, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );

        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("MIG29", "d:\\Media\\3DModels\\OBJ\\Mig-29_Fulcrum\\Mig-29_Fulcrum.obj");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.25f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(-10.0f, 14.0f, -28.0f) );
        pNode->SetRotations(-90.0f, 120.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("F35", "d:\\Media\\3DModels\\OBJ\\F-35_Lightning_II\\F-35_Lightning_II.obj");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.25f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(0.0f, 15.0f, -30.0f) );
        pNode->SetRotations(-90.0f, 120.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);

        ////////////////////////////////////////////////////////////////////
        SceneNode* pJet2 = new SceneNode( pModel3d );
        pJet2->SetTrans( glm::vec3(1.0f, 5.0f, 2.0f) );
        pJet2->SetScale( scale );
        pNode->AddChild(pJet2);
    }

#ifndef _DEBUG
    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("GreenLShapedBlock", "d:\\Media\\3DModels\\Buildings\\Eclectic\\Building N150509.3ds");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        // scale then translate model to origin then rotate, then translate it someplace else in the world
        scale = 0.00025f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(-9.0f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -15.0f) );
        pNode->SetRotations(180.0f, 90.0f, 90.0f);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load( "Dali", "d:\\Media\\3DModels\\Buildings\\Eclectic\\Building N050510.3DS" );
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.005f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(-15.5f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -4.0f) );
        pNode->SetRotations(90.0f, 0, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load( "Mall", "d:\\Media\\3DModels\\Buildings\\Eclectic\\Building_N270810.3DS" );
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.0002f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(-8.0f, (bbox._max.z - bbox._min.z) * 0.5f * scale, 4.5f) );
        pNode->SetRotations(90.0f, 0, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("VW", "d:\\Media\\3DModels\\Cars\\Volskwagen N180814.3ds");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.0075f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(0, (bbox._max.z - bbox._min.z) * 0.5f * scale, 0) + glm::vec3(-0.3f, 0.025f, -2.0f) );
        pNode->SetRotations(90.0f, 90.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("FlatNew", "d:\\Media\\3DModels\\Buildings\\Eclectic\\Building N020509.3ds");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.00025f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(-3.5f, (bbox._max.z - bbox._min.z) * 0.5f * scale, 14.0f) );
        pNode->SetRotations(90.0f, 230.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("Villa3Stories", "d:\\Media\\3DModels\\Buildings\\Apartment\\city3.obj");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.2f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(4.0f, (bbox._max.y - bbox._min.y) * 0.5f * scale, -3.0f) );
        pNode->SetRotations(0, 180.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("OfficeVerticalWindows", "d:\\Media\\3DModels\\Buildings\\Building11\\Build11.3ds");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.025f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(13.0f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -3.5f) );
        pNode->SetRotations(90.0f, 0, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("OfficeLShaped", "d:\\Media\\3DModels\\Buildings\\Building10\\Build10.3ds");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.025f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(6.5f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -8.0f) );
        pNode->SetRotations(90.0f, 0, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("SovietBlock", "d:\\Media\\3DModels\\Buildings\\Building06\\building06.lwo");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.25f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(4.25f, (bbox._max.y - bbox._min.y) * 0.5f * scale, 9.0f) );
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

#ifdef _LARGE_MODELS
    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("OfficeCurvy", "d:\\Media\\3DModels\\Buildings\\Eclectic\\Building N090914.3DS");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.0002f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(-6.5f, (bbox._max.z - bbox._min.z) * 0.5f * scale, -4.0f) );
        pNode->SetRotations(90.0f, 60.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("Flat4Stories", "d:\\Media\\3DModels\\Buildings\\Eclectic\\BuildingEclectic.3ds");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        scale = 0.00015f;

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(11.0f, (bbox._max.z - bbox._min.z) * 0.5f * scale, 2.8f) );
        pNode->SetRotations(90.0f, 210.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }
#endif // _LARGE_MODELS
#endif
    //////////////////////////// Roads /////////////////////////////////
    scale = 0.00075f;

    pModel3d = loader.Load("RoadXJunction", "d:\\Media\\3DModels\\Roads\\Road-2-Lane-X.obj");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();

        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3(0, -bbox._min.y * scale, 0) );
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    ////////////////////////////////////////////////////////////////////
    pModel3d = loader.Load("RoadTile", "d:\\Media\\3DModels\\Roads\\Road-2-Lane-Straight.obj");
    if ( pModel3d )
    {
        bbox = pModel3d->GetBBox();
        center = bbox.center();
    }

    int numTiles = 10;
    // from origin to East
    for (int i=1; i<=numTiles; ++i)
    {
        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3( i * (bbox._max.x - bbox._min.x) * scale, -bbox._min.y * scale, 0.0f ) );
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    // from origin to West
    for (int i=1; i<=numTiles; ++i)
    {
        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3( -i * (bbox._max.x - bbox._min.x) * scale, -bbox._min.y * scale, 0.0f ) );
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    // from origin to South
    for (int i=1; i<=numTiles; ++i)
    {
        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3( 0.0f, -bbox._min.y * scale, i * (bbox._max.z - bbox._min.z) * scale ) );
        pNode->SetRotations(0, 270.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    // from origin to North
    for (int i=1; i<=numTiles; ++i)
    {
        pNode = new SceneNode( pModel3d );
        pNode->SetTrans( glm::vec3( 0.0f, -bbox._min.y * scale, -i * (bbox._max.z - bbox._min.z) * scale ) );
        pNode->SetRotations(0, 270.0f, 0);
        pNode->SetOrigin( center );
        pNode->SetScale( scale );
        AddChild(pNode);
    }

    return true;
}
