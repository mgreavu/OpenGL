#pragma once
#include "common\SceneNode.h"
#include <vector>
#include <memory>

using std::vector;
using std::shared_ptr;

class SceneGraph : public SceneNode
{
public:
    SceneGraph();
    virtual ~SceneGraph();

    bool Load();
    SceneNode* GetRoot();

    template <typename T>
    SceneNode* Find( SceneNode* pFrom, const T& );
};
