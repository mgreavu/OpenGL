#include "stdafx.h"
#include "Geometry.h"
#include "GeometryDlg.h"
#include "AboutDlg.h"
#include "afxdialogex.h"
#include "DrawingSurfaceSim.h"
#include "common/camera/ProjectorFPS.h"
#include "SceneGraph.h"
#include "CameraVectorsDlg.h"
#include "SceneGraphDlg.h"
#include <glm/gtc/matrix_transform.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CGeometryDlg::CGeometryDlg( CWnd* pParent /*=NULL*/ )
    : CDialogEx( CGeometryDlg::IDD, pParent )
    , m_cullMode( CullMode::CULL_BACK )
    , m_flags( RenderFlags::FLAGS_NONE )
    , m_pTarget( NULL )
    , m_t( 0 )
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CGeometryDlg::~CGeometryDlg()
{
    dbg("~CGeometryDlg()\r\n");
}

void CGeometryDlg::OnCancel()
{
    dbg("CGeometryDlg::OnCancel()\r\n");

    KillTimer( 6543 );
    m_pDrawingSurface->Release();

    CDialogEx::OnCancel();
}

void CGeometryDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CGeometryDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_WM_SIZE()
    ON_COMMAND(ID_FILE_EXIT, &CGeometryDlg::OnFileExit)
    ON_COMMAND(ID_FILE_OPENGL11, &CGeometryDlg::OnFileOpengl11)
    ON_COMMAND(ID_FILE_OPENGL3X, &CGeometryDlg::OnFileOpengl3x)
    ON_COMMAND(ID_FILE_LOAD, &CGeometryDlg::OnFileLoad)
    ON_COMMAND(ID_HELP_ABOUT, &CGeometryDlg::OnHelpAbout)
    ON_COMMAND(ID_TOOLS_CAMERA, &CGeometryDlg::OnToolsCamera)
    ON_COMMAND(ID_TOOLS_SCENEGRAPH, &CGeometryDlg::OnToolsScenegraph)
    ON_WM_TIMER()
END_MESSAGE_MAP()


// CGeometryDlg message handlers

BOOL CGeometryDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    SetIcon(m_hIcon, TRUE);
    SetIcon(m_hIcon, FALSE);

    if ( !m_menu.LoadMenu(IDR_MENU_MAIN) )
    {
        AfxThrowResourceException();
    }
    SetMenu( &m_menu );

    MoveWindow( CRect(CPoint(0, 0), CPoint(1066, 600)), FALSE );

    m_pProj.reset( new ProjectorFPS( 60.0f, 0.2f, 400.0f ) );
    m_pProj->Update();  // virtual, avoid calling it in the constructor
    m_pProj->MoveUp(2.0f);  // elevate ourselves a bit over the origin

    OnFileOpengl3x();

    return TRUE;
}

void CGeometryDlg::OnFileLoad()
{
    if (!( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) ))
    {
        return;
    }

    m_pSceneGraph.reset( new SceneGraph() );
    if ( m_pSceneGraph->Load() )
    {
        // SceneGraphPtr converted to SceneNodePtr
        m_pDrawingSurface->SetScene( m_pSceneGraph );

        m_pTarget = m_pSceneGraph->Find( m_pSceneGraph->GetRoot(), std::string("F35") );

        SetTimer( 6543, 16, NULL );

        m_menu.EnableMenuItem(ID_FILE_LOAD, MF_GRAYED);
    }
}

void CGeometryDlg::OnTimer(UINT_PTR nIDEvent)
{
    if ( nIDEvent == 6543 )
    {
        if ( m_pTarget )
        {
            ( m_t > 360.0f ) ? m_t = 0 : m_t += 0.05f;

            // ellipse parametric equation
            float r1 = 50.0f, r2 = 150.0f;
            float x = r1 * glm::cos( glm::radians(m_t) );
            float z = -50.0f + r2 * glm::sin( glm::radians(m_t) );

            m_pTarget->SetTrans( glm::vec3( x, 10.0f, z) );
            m_pTarget->SetRotations( -90.0f, -180.0f - m_t, 0 );
        }
    }

    CDialogEx::OnTimer(nIDEvent);
}

bool CGeometryDlg::InitDS()
{
    m_pDrawingSurface->SetProjector( m_pProj );

    CRect rcClient;
    GetClientRect(rcClient);
    bool bOk = ( m_pDrawingSurface->Create( NULL, NULL, WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE, rcClient, this, IDC_CANVAS ) != -1);
    if ( bOk )
    {
        m_flags = RenderFlags::FLAG_ENABLE_LIGHTS | RenderFlags::FLAG_ENBLE_TEXTURES;
        m_pDrawingSurface->SetFlags(m_flags);
        m_pDrawingSurface->SetCullMode( CullMode::CULL_BACK );

        if ( m_pSceneGraph )
        {
            // SceneGraphPtr converted to SceneNodePtr
            m_pDrawingSurface->SetScene( m_pSceneGraph );
        }

        m_pDrawingSurface->EnableRefresh( true );
        m_pDrawingSurface->SetFocus();
    }
    return bOk;
}

void CGeometryDlg::OnFileOpengl11()
{
    m_menu.EnableMenuItem(ID_FILE_OPENGL3X, MF_ENABLED);
    m_menu.EnableMenuItem(ID_FILE_OPENGL11, MF_GRAYED);

    if ( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) )
    {
        m_pDrawingSurface->Release();
        m_pDrawingSurface->DestroyWindow();
    }

#ifdef _DEBUG
    m_pDrawingSurface.reset( new DrawingSurfaceSim(50, DrawingSurface::CONTEXT_11) );
#else
    m_pDrawingSurface.reset( new DrawingSurfaceSim(16, DrawingSurface::CONTEXT_11) ); // higher fps
#endif
    InitDS();
}

void CGeometryDlg::OnFileOpengl3x()
{
    m_menu.EnableMenuItem(ID_FILE_OPENGL3X, MF_GRAYED);
    m_menu.EnableMenuItem(ID_FILE_OPENGL11, MF_ENABLED);

    if ( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) )
    {
        m_pDrawingSurface->Release();
        m_pDrawingSurface->DestroyWindow();
    }
#ifdef _DEBUG
    m_pDrawingSurface.reset( new DrawingSurfaceSim(50, DrawingSurface::CONTEXT) );
#else
    m_pDrawingSurface.reset( new DrawingSurfaceSim(16, DrawingSurface::CONTEXT) ); // higher fps
#endif
    InitDS();
}

void CGeometryDlg::OnSize(UINT nType, int cx, int cy)
{
    CDialogEx::OnSize(nType, cx, cy);

    if ( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) )
    {
        CRect rcClient;
        GetClientRect(rcClient);
        m_pDrawingSurface->MoveWindow(rcClient);
    }
}

BOOL CGeometryDlg::PreTranslateMessage(MSG* pMsg)
{
    if (pMsg->message == WM_CHAR)
    {
        switch (pMsg->wParam)
        {
        case 'b':
        case 'B':
            {
                m_flags = m_pDrawingSurface->GetFlags();
                if ( m_flags & RenderFlags::FLAG_DRAW_BBOX )
                {
                    m_flags &= ~RenderFlags::FLAG_DRAW_BBOX;
                }
                else
                {
                    m_flags |= RenderFlags::FLAG_DRAW_BBOX;
                }
                m_pDrawingSurface->SetFlags( m_flags );
                break;
            }
        case 'm':
        case 'M':
            {
                m_flags = m_pDrawingSurface->GetFlags();
                if ( m_flags & RenderFlags::FLAG_DRAW_WIREFRAME )
                {
                    m_flags &= ~RenderFlags::FLAG_DRAW_WIREFRAME;
                }
                else
                {
                    m_flags |= RenderFlags::FLAG_DRAW_WIREFRAME;
                }
                m_pDrawingSurface->SetFlags( m_flags );
                break;
            }
        case 't':
        case 'T':
            {
                m_flags = m_pDrawingSurface->GetFlags();
                if ( m_flags & RenderFlags::FLAG_ENBLE_TEXTURES )
                {
                    m_flags &= ~RenderFlags::FLAG_ENBLE_TEXTURES;
                }
                else
                {
                    m_flags |= RenderFlags::FLAG_ENBLE_TEXTURES;
                }
                m_pDrawingSurface->SetFlags( m_flags );
                break;
            }
        case 'l':
        case 'L':
            {
                m_flags = m_pDrawingSurface->GetFlags();
                if ( m_flags & RenderFlags::FLAG_ENABLE_LIGHTS )
                {
                    m_flags &= ~RenderFlags::FLAG_ENABLE_LIGHTS;
                }
                else
                {
                    m_flags |= RenderFlags::FLAG_ENABLE_LIGHTS;
                }
                m_pDrawingSurface->SetFlags( m_flags );
                break;
            }
        case 'f':
        case 'F':
            {
                if (++m_cullMode > CullMode::CULL_FRONT ) { m_cullMode = CullMode::CULL_NONE; }
                switch ( m_cullMode )
                {
                case 0: m_pDrawingSurface->SetCullMode( CullMode::CULL_NONE ); break;
                case 1: m_pDrawingSurface->SetCullMode( CullMode::CULL_BACK ); break;
                case 2: m_pDrawingSurface->SetCullMode( CullMode::CULL_FRONT ); break;
                }
                break;
            }
        case 'c':
        case 'C':
            {
                OnToolsCamera();
                return TRUE;
            }
        case 'g':
        case 'G':
            {
                OnToolsScenegraph();
                return TRUE;
            }
        }
        return FALSE;
    }

    return CDialogEx::PreTranslateMessage(pMsg);
}

void CGeometryDlg::OnToolsCamera()
{
    CameraVectorsDlg dlgCamVec( this, m_pProj->GetCameraPosition(), m_pProj->GetCameraTarget(), m_pProj->GetCameraUp() );
    dlgCamVec.DoModal();
}

void CGeometryDlg::OnToolsScenegraph()
{
    SceneGraphDlg dlg(this, m_pProj->GetModelViewMatrix(), m_pSceneGraph);
    dlg.DoModal();
}

void CGeometryDlg::OnFileExit()
{
    OnCancel();
}

BOOL CGeometryDlg::OnEraseBkgnd(CDC* pDC)
{
    return ( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) ) ? TRUE : CDialogEx::OnEraseBkgnd( pDC );
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGeometryDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

void CGeometryDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGeometryDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void CGeometryDlg::OnHelpAbout()
{
    CAboutDlg dlgAbout;
    dlgAbout.DoModal();
}
