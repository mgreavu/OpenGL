#pragma once
#include "common/renderers/SceneGraphRenderer.h"
#include "common/context/GLContext11.h"
#include "common/FastFrustumCulling.h"
#include "common/Grid.h"

class GLContext11;
class IProjector;

class SGRendererGL11 : public ISceneGraphRenderer
{
public:
    SGRendererGL11();
    virtual ~SGRendererGL11();

    bool Init( GLContext11* pGLCtx, const shared_ptr<IProjector>& proj );
    void SetFlags( int flags );
    void SetCullMode( CullMode mode );
    void SetScene( const shared_ptr<SceneNode>& pScene );
    void Render();
    void RemoveScene();
    void CleanUp();
    void Resize( int cx, int cy );

protected:
    void PrepareScene( SceneNode* pScene );
    void RenderScene( SceneNode* pScene );
    void RemoveScene( SceneNode* pScene );

    void InitLights();
    void InitDefaultMaterial();
    void InitTextures();
    void RenderCoordSystem();
    void RenderBBox( const BoundingBox& bbox, const glm::vec4& color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f) );

    GLContext11* m_pGLCtx;
    shared_ptr<IProjector> m_pProj;
    int m_flags;

    bool  m_bLights; // helper to type less m_bFlags testing
    const glm::vec4 m_lightPos;

    Grid m_MeshGrid;

    FastFrustumCulling m_ffCuller;

    shared_ptr<SceneNode> m_pScene;
};

