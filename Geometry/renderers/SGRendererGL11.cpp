#include "stdafx.h"
#include "SGRendererGL11.h"
#include "common/camera/Projector.h"
#include "common/model3d/Model3d.h"
#include "common/model3d/Material.h"
#include "common/model3d/Texture.h"
#include "common/SceneNode.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>


SGRendererGL11::SGRendererGL11()
    : m_pGLCtx( NULL)
    , m_flags( RenderFlags::FLAGS_NONE )
    , m_lightPos(-5.0f, 5.0f, 10.0f, 0.0f)
{
    m_bLights = (m_flags & RenderFlags::FLAG_ENABLE_LIGHTS) ? true : false;
}

SGRendererGL11::~SGRendererGL11()
{
}

bool SGRendererGL11::Init( GLContext11* pGLCtx, const shared_ptr<IProjector>& proj )
{
    m_pGLCtx = pGLCtx;
    m_pProj = proj;

    bool bOk = m_pGLCtx->Select();
    if ( bOk )
    {
        glEnable(GL_DEPTH_TEST);

        InitDefaultMaterial();
        InitLights();
        InitTextures();

        glEnableClientState(GL_VERTEX_ARRAY);
    }
    return bOk;
}

void SGRendererGL11::CleanUp()
{
    if (m_pGLCtx != NULL)
    {
        if (::wglGetCurrentContext() != NULL)
        {
            RemoveScene();
            glDisableClientState(GL_VERTEX_ARRAY);
        }
    }
}

/*
    So, there are three light colors for each light - Ambient, Diffuse and Specular (set with glLight) and four for each surface (set with glMaterial).
    All OpenGL implementations support at least eight light sources - and the glMaterial can be changed at will for each polygon
    (although there are typically large time penalties for doing that - so we'd like to minimize the number of changes).

    The final polygon color is the sum of all four light components, each of which is formed by multiplying the glMaterial color by the glLight color 
    (modified by the directionality in the case of Diffuse and Specular). Since there is no Emission color for the glLight, that is added to 
    the final color without modification.

    A good set of settings for a light source would be to set the Diffuse and Specular components to the color of the light source, and the Ambient to 
    the same color - but at MUCH reduced intensity, 10% to 40% seems reasonable in most cases.

    For the glMaterial, it's usual to set the Ambient and Diffuse colors to the natural color of the object and to put the Specular color to white. 
    The emission color is generally black for objects that do not shine by their own light.

    Before you can use an OpenGL light source, it must be positioned using the glLight command and enabled using glEnable(GL_LIGHTn) where 'n' is 0 through 7. 
    There are additional commands to make light sources directional (like a spotlight or a flashlight) and to have it attenuate as a function of range from the light source. 
*/
void SGRendererGL11::InitLights()
{
/*
    glClearColor() sets the color (and alpha value) a RGBA framebuffer will be cleared to with glClear(GL_COLOR_BUFFER_BIT).
    The framebuffer alpha value would be taken into account with GL_DST_ALPHA or GL_ONE_MINUS_DST_ALPHA blend functions.
    However your regular on-screen window framebuffer usually doesn't carry an alpha value. The initial values are all 0.
    Framebuffers with an alpha channel are mostly of interest als render to texture targets.
    So in your case, the clear color is of no particular interest for you.
    You're much more likely interested in the 4th parameter of glColor4f or textures with an alpha channel.
*/
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);

    glShadeModel(GL_SMOOTH);                // Gourand shading can be turned on by GL_SMOOTH ( default ) or off by GL_FLAT

    GLfloat ambient_color[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    GLfloat diffuse_color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat specular_color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
/*
    We can add an ambient light for all the scene with GL_LIGHT_MODEL_AMBIENT.
    The particularity of this ambient light is that it comes from any source.
    In addition, this light is activated by GL_LIGHTING so you don't have to enable any GL_LIGHTi to use it.
*/
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_color);

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_color);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_color);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_color);
/*
    if w != 0, => positional light at position ( x/w, y/w, z/w )
    if w = 0, => directional light ( light source at infinity ), (x, y, z) is the direction of the source
    By default, GL_POSITION is (0, 0, 1, 0), which defines a directional light that points along the negative z-axis.
*/
    glLightfv(GL_LIGHT0, GL_POSITION, glm::value_ptr(m_lightPos));  // the light position is transformed by the ModelView that's active when you call glLightfv()
}

/*
    Specular reflection from an object produces highlights. Unlike ambient and diffuse reflection, the amount of specular reflection seen by a viewer does depend
    on the location of the viewpoint - it's brightest along the direct angle of reflection. To see this, imagine looking at a metallic ball outdoors in the sunlight.
    As you move your head, the highlight created by the sunlight moves with you to some extent. However, if you move your head too much, you lose the highlight entirely.

    OpenGL allows you to set the effect that the material has on reflected light (with GL_SPECULAR) and control the size and brightness of the highlight (with GL_SHININESS).
    You can assign a number in the range of [0.0, 128.0] to GL_SHININESS - the higher the value, the smaller and brighter (more focused) the highlight.
*/
void SGRendererGL11::InitDefaultMaterial()
{
    GLfloat Ks[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glMaterialfv(GL_FRONT, GL_SPECULAR, Ks);

    GLfloat mat_shininess = 50.0f;  // the smaller, the brighter
    glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

/*
    For glTexEnvf(), the state is not reset when you disable texturing.
    glEnable(GL_TEXTURE_2D) has no effect on any of the other texturing functions,
    it just determines whether or not to sample the texture during a draw call.
    You don't have to enable texturing to set a texture parameter.
*/
void SGRendererGL11::InitTextures()
{
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void SGRendererGL11::Resize( int cx, int cy )
{
    m_pProj->SetViewport( glm::i32vec2(cx, cy) );
    glViewport(0, 0, cx, cy);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf( glm::value_ptr(m_pProj->GetProjectionMatrix()) );

    glMatrixMode(GL_MODELVIEW);
}

void SGRendererGL11::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);  // Sets the current color

    glLoadMatrixf( glm::value_ptr( m_pProj->GetModelViewMatrix() ) );
    if (m_bLights)
    {
        glLightfv( GL_LIGHT0, GL_POSITION, glm::value_ptr(m_lightPos) );
    }

    RenderCoordSystem();

    if ( m_pScene )
    {
        RenderScene( m_pScene.get() );
    }

    m_pGLCtx->SwapBuffers();
}

void SGRendererGL11::RenderScene( SceneNode* pScene )
{
    const Model3d* pModel = pScene->GetModel();
    if ( pModel )
    {
        glm::mat4 matAll = m_pProj->GetModelViewMatrix()    // view transform
                         * pScene->GetWorldTransform()      // world transform
                         * glm::scale( glm::mat4(1.0f), pScene->GetScale() );   // this scale

        glLoadMatrixf( glm::value_ptr( matAll ) );

        glm::mat4 matMVP = m_pProj->GetProjectionMatrix() * matAll;
        m_ffCuller.Load( matMVP );

        for ( Model3d::ConstIter itMesh = pModel->GetMeshesBegin();
            itMesh != pModel->GetMeshesEnd(); ++itMesh )
        {
            // frustum culling
            if ( m_ffCuller.RectPrismCulled( (*itMesh).m_bbox ) == FFC_CULLED )
            {
//              dbg("Culled submesh in '%s'\r\n", pModel->GetName().c_str());
                continue;
            }

            const Material* pMaterial = (*itMesh).m_pMaterial.get();
            ASSERT( pMaterial );

            if ( m_bLights )
            {
                if (pMaterial->IsAlpha())
                {
                    glEnable(GL_BLEND);
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                }

                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, pMaterial->m_Ka);
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, pMaterial->m_Kd);
                glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, pMaterial->m_Ks);
                glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, pMaterial->m_shininess);

                glNormalPointer(GL_FLOAT, sizeof(Vertex), &(*itMesh).m_Vertices[0].m_norm);
                glEnableClientState(GL_NORMAL_ARRAY);
            }
            else
            {
                glColor4f(pMaterial->m_Kd[0], pMaterial->m_Kd[1], pMaterial->m_Kd[2], pMaterial->m_Kd[3]); // GL_COLOR_MATERIAL is enabled
            }

            if ( (m_flags & RenderFlags::FLAG_ENBLE_TEXTURES) &&  pMaterial->m_pTexture )
            {
                glBindTexture( pMaterial->m_pTexture->GetTexTarget(), pMaterial->m_pTexture->GetTexObj() );
                glEnable(GL_TEXTURE_2D);
                glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &(*itMesh).m_Vertices[0].m_tex);
                glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            }

            glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &(*itMesh).m_Vertices[0]);
            glDrawElements(GL_TRIANGLES, (*itMesh).m_Indices.size(), GL_UNSIGNED_INT, &(*itMesh).m_Indices[0]);

            if ( (m_flags & RenderFlags::FLAG_ENBLE_TEXTURES) &&  pMaterial->m_pTexture )
            {
                glDisableClientState(GL_TEXTURE_COORD_ARRAY);
                glDisable(GL_TEXTURE_2D);
            }

            if ( m_bLights )
            {
                glDisableClientState(GL_NORMAL_ARRAY);

                if (pMaterial->IsAlpha())
                {
                    glDisable(GL_BLEND);
                }
            }

            if ( m_flags & RenderFlags::FLAG_DRAW_BBOX ) { RenderBBox( (*itMesh).m_bbox ); }
        }

        if ( m_flags & RenderFlags::FLAG_DRAW_BBOX ) { RenderBBox( pModel->GetBBox(), glm::vec4(0.0f, 0.0f, 0.75f, 1.0f) ); }
    }

    for ( SceneNode::ConstIter it = pScene->GetChildrenBegin();
        it != pScene->GetChildrenEnd(); ++it )
    {
        RenderScene( *it );
    }
}

void SGRendererGL11::RenderBBox( const BoundingBox& bbox, const glm::vec4& color )
{
    vector<Vertex> veCorners;
    veCorners.reserve(16);

    veCorners.push_back(Vertex(bbox.corner(0)));
    veCorners.push_back(Vertex(bbox.corner(4)));
    veCorners.push_back(Vertex(bbox.corner(6)));
    veCorners.push_back(Vertex(bbox.corner(2)));

    veCorners.push_back(Vertex(bbox.corner(1)));
    veCorners.push_back(Vertex(bbox.corner(5)));
    veCorners.push_back(Vertex(bbox.corner(7)));
    veCorners.push_back(Vertex(bbox.corner(3)));

    veCorners.push_back(Vertex(bbox.corner(2)));
    veCorners.push_back(Vertex(bbox.corner(6)));
    veCorners.push_back(Vertex(bbox.corner(7)));
    veCorners.push_back(Vertex(bbox.corner(3)));

    veCorners.push_back(Vertex(bbox.corner(0)));
    veCorners.push_back(Vertex(bbox.corner(4)));
    veCorners.push_back(Vertex(bbox.corner(5)));
    veCorners.push_back(Vertex(bbox.corner(1)));

    if (m_bLights) { glDisable(GL_LIGHTING); }
    glColor4f(color.x, color.y, color.z, color.w);

    glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &veCorners[0]);
    for (unsigned int i=0; i<16; i+=4)
    {
        glDrawArrays(GL_LINE_LOOP, i, 4);
    }
    if (m_bLights) { glEnable(GL_LIGHTING); }
}

void SGRendererGL11::RenderCoordSystem()
{
    if (m_bLights) { glDisable(GL_LIGHTING); }

    glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &m_MeshGrid.m_vertices[0]);

    // draw the axes through origin with a different color
    glColor4f(0.5f, 0.0f, 0.25f, 1.0f);
    glDrawArrays(GL_LINES, 0, 4);

    // draw the grid
    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glDrawArrays(GL_LINES, 4, 2 * (4 * Grid::HALF_SIDE + 2) - 4);

    if (m_bLights) { glEnable(GL_LIGHTING); }
}

void SGRendererGL11::SetScene( const shared_ptr<SceneNode>& pScene )
{
    m_pScene = pScene;

    if ( m_pScene )
    {
        PrepareScene( m_pScene.get() );
    }
}

void SGRendererGL11::RemoveScene()
{
    if ( m_pScene )
    {
        RemoveScene( m_pScene.get() );
        m_pScene.reset();
    }
}

void SGRendererGL11::PrepareScene( SceneNode* pScene )
{
    const Model3d* pModel = pScene->GetModel();
    if ( pModel )
    {
        for ( Model3d::ConstIter itMesh = pModel->GetMeshesBegin();
            itMesh != pModel->GetMeshesEnd(); ++itMesh )
        {
            ASSERT( (*itMesh).m_pMaterial );
            Texture* pTex = (*itMesh).m_pMaterial->m_pTexture.get();
            if ( pTex && !pTex->IsReady() )
            {
                GLuint texObj;
                glGenTextures( 1, &texObj );
                pTex->SetTexObj( texObj );

                GLenum texTarget = pTex->GetTexTarget();

                glBindTexture(texTarget, texObj);
//              glTexParameteri(texTarget, GL_TEXTURE_WRAP_S, GL_REPEAT);
//              glTexParameteri(texTarget, GL_TEXTURE_WRAP_T, GL_REPEAT);
                glTexParameteri(texTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(texTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                unsigned int cols, rows;
                const void* pData = pTex->GetData(cols, rows);
                glTexImage2D(texTarget, 0, GL_RGBA, cols, rows, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData);

                pTex->SetReady(true);

                dbg("PrepareScene() --> texTarget = %d, texObj = %d\r\n", pTex->GetTexTarget(), pTex->GetTexObj());
            }
        }
    }

    for ( SceneNode::ConstIter it = pScene->GetChildrenBegin();
        it != pScene->GetChildrenEnd(); ++it )
    {
        PrepareScene(*it);
    }
}

void SGRendererGL11::RemoveScene( SceneNode* pScene )
{
    const Model3d* pModel = pScene->GetModel();
    if ( pModel )
    {
        for ( Model3d::ConstIter itMesh = pModel->GetMeshesBegin();
            itMesh != pModel->GetMeshesEnd(); ++itMesh )
        {
            Texture* pTex = (*itMesh).m_pMaterial->m_pTexture.get();
            if ( pTex && pTex->IsReady() )
            {
                GLuint texObj = pTex->GetTexObj();
                glDeleteTextures(1, &texObj);

                pTex->SetReady(false);
            }
        }
    }

    for ( SceneNode::ConstIter it = pScene->GetChildrenBegin();
        it != pScene->GetChildrenEnd(); ++it )
    {
        RemoveScene(*it);
    }
}

void SGRendererGL11::SetFlags( int flags )
{
    if ( (flags & RenderFlags::FLAG_DRAW_WIREFRAME) != (m_flags & RenderFlags::FLAG_DRAW_WIREFRAME) )
    {
        (flags & RenderFlags::FLAG_DRAW_WIREFRAME) ? m_pGLCtx->SetPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
                                                   : m_pGLCtx->SetPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }

    if ( (flags & RenderFlags::FLAG_ENABLE_LIGHTS) != (m_flags & RenderFlags::FLAG_ENABLE_LIGHTS) )
    {
        if (flags & RenderFlags::FLAG_ENABLE_LIGHTS)
        {
            glDisable(GL_COLOR_MATERIAL);
            glEnable(GL_NORMALIZE);
            glEnable(GL_LIGHT0);
            glEnable(GL_LIGHTING);
/*
    OpenGL needs the normals to have a magnitude of 1 to get lighting right.
    First, if your matrix never scales, you don't need to normalize at all, which is the fastest normal transformation obviously.
    This will happen if your 4x4 matrix "simply" rotates and translates objects in your scene.
    Secondly, if your scaling is uniform (e.g. every time you scale *2 along the X axis, you also scale Y axis *2 and Z axis *2), you may enable GL_RESCALE_NORMAL.

    Thirdly, if your matrix has no limit and can perform non-uniform scaling, you must enable NORMALIZE, which is the slowest normal transformation.
    You can also normalize in a vertex shader, but the performance cost is roughly the same.
    Many games never fall into the third case, as it is uncommon to let an artist shape a model with a specific design and distort it non-uniformly.
    GL_RESCALE_NORMAL is 1.2 and the Microsoft implementation is only 1.1.
*/
        }
        else
        {
            glDisable(GL_LIGHTING);
            glDisable(GL_LIGHT0);
            glDisable(GL_NORMALIZE);
            glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE); // Call glColorMaterial() before enabling GL_COLOR_MATERIAL
            glEnable(GL_COLOR_MATERIAL);
        }
    }

    m_bLights = (flags & RenderFlags::FLAG_ENABLE_LIGHTS) ? true : false;

    m_flags = flags;
}

void SGRendererGL11::SetCullMode( CullMode mode )
{
    switch( mode )
    {
    case CULL_NONE:
        {
            glDisable(GL_CULL_FACE);
            break;
        }
    case CULL_BACK:
        {
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);
            break;
        }
    case CULL_FRONT:
        {
            glCullFace(GL_FRONT);
            glEnable(GL_CULL_FACE);
            break;
        }
    }
}
