#include "stdafx.h"
#include "Geometry.h"
#include "SceneGraphDlg.h"
#include "afxdialogex.h"
#include "common/SceneNode.h"
#include "common/model3d/Model3d.h"
#include <glm/gtc/matrix_transform.hpp>


IMPLEMENT_DYNAMIC(SceneGraphDlg, CDialogEx)

SceneGraphDlg::SceneGraphDlg( CWnd* pParent, const glm::mat4& matView, const shared_ptr<SceneNode>& sceneGraph )
    : CDialogEx( SceneGraphDlg::IDD, pParent )
    , m_matView( matView )
    , m_SceneGraph( sceneGraph )
    , m_m00(0)
    , m_m01(0)
    , m_m02(0)
    , m_m03(0)
    , m_m10(0)
    , m_m11(0)
    , m_m12(0)
    , m_m13(0)
    , m_m20(0)
    , m_m21(0)
    , m_m22(0)
    , m_m23(0)
    , m_m30(0)
    , m_m31(0)
    , m_m32(0)
    , m_m33(0)
    , m_mv00(0)
    , m_mv01(0)
    , m_mv02(0)
    , m_mv03(0)
    , m_mv10(0)
    , m_mv11(0)
    , m_mv12(0)
    , m_mv13(0)
    , m_mv20(0)
    , m_mv21(0)
    , m_mv22(0)
    , m_mv23(0)
    , m_mv30(0)
    , m_mv31(0)
    , m_mv32(0)
    , m_mv33(0)
{
}

SceneGraphDlg::~SceneGraphDlg()
{
}

void SceneGraphDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_SCENEGRAPH, m_ctrlSceneGraph);

    DDX_Text_Formatted(pDX, IDC_M00, m_m00, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M01, m_m01, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M02, m_m02, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M03, m_m03, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M10, m_m10, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M11, m_m11, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M12, m_m12, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M13, m_m13, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M20, m_m20, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M21, m_m21, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M22, m_m22, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M23, m_m23, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M30, m_m30, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M31, m_m31, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M32, m_m32, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_M33, m_m33, _T("%.5lf"));

    DDX_Text_Formatted(pDX, IDC_MV00, m_mv00, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV01, m_mv01, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV02, m_mv02, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV03, m_mv03, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV10, m_mv10, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV11, m_mv11, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV12, m_mv12, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV13, m_mv13, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV20, m_mv20, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV21, m_mv21, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV22, m_mv22, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV23, m_mv23, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV30, m_mv30, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV31, m_mv31, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV32, m_mv32, _T("%.5lf"));
    DDX_Text_Formatted(pDX, IDC_MV33, m_mv33, _T("%.5lf"));
}

BEGIN_MESSAGE_MAP(SceneGraphDlg, CDialogEx)
    ON_BN_CLICKED(IDOK, &SceneGraphDlg::OnBnClickedOk)
    ON_NOTIFY(TVN_SELCHANGED, IDC_SCENEGRAPH, &SceneGraphDlg::OnTvnSelchangedScenegraph)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


BOOL SceneGraphDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    HTREEITEM hRoot = Insert( NULL, m_SceneGraph.get() );
    m_ctrlSceneGraph.SelectItem( hRoot );

    return TRUE;
}

HTREEITEM SceneGraphDlg::Insert( HTREEITEM hParent, SceneNode* pNode )
{
    if ( !pNode )
    {
        return hParent;
    }

    string name("(no model)");

    const Model3d* pModel = pNode->GetModel();
    if ( pModel && pModel->GetName().size() )
    {
        name = pModel->GetName();
    }

    size_t newsize = name.size();
    wchar_t* pwcstring = new wchar_t[newsize + 1];
    memset(pwcstring, 0, sizeof(wchar_t) * (newsize + 1));
    size_t convertedChars = 0;
    mbstowcs_s(&convertedChars, pwcstring, newsize + 1, name.c_str(), newsize);

    hParent = m_ctrlSceneGraph.InsertItem( TVIF_TEXT, pwcstring, 0, 0, 0, 0, 0, hParent, NULL );
    m_ctrlSceneGraph.SetItemData( hParent, reinterpret_cast<DWORD_PTR>(pNode) );

    delete [] pwcstring;

    for ( SceneNode::ConstIter it = pNode->GetChildrenBegin();
        it != pNode->GetChildrenEnd(); ++it )
    {
        Insert( hParent, (*it) );
        m_ctrlSceneGraph.Expand( hParent, TVE_EXPAND );
    }
    return hParent;
}

void SceneGraphDlg::OnTvnSelchangedScenegraph(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
    
    if ( pNMTreeView )
    {
        SceneNode* pNode = reinterpret_cast<SceneNode*>(m_ctrlSceneGraph.GetItemData( pNMTreeView->itemNew.hItem ));
        if ( pNode )
        {
            UpdateMatrix( pNode );
        }
    }

    *pResult = 0;
}

void SceneGraphDlg::UpdateMatrix( const SceneNode* pNode )
{
    glm::mat4 mat = pNode->GetWorldTransform() * glm::scale( glm::mat4(1.0f), pNode->GetScale() );

    m_m00 = mat[0][0];
    m_m01 = mat[0][1];
    m_m02 = mat[0][2];
    m_m03 = mat[0][3];

    m_m10 = mat[1][0];
    m_m11 = mat[1][1];
    m_m12 = mat[1][2];
    m_m13 = mat[1][3];

    m_m20 = mat[2][0];
    m_m21 = mat[2][1];
    m_m22 = mat[2][2];
    m_m23 = mat[2][3];

    m_m30 = mat[3][0];
    m_m31 = mat[3][1];
    m_m32 = mat[3][2];
    m_m33 = mat[3][3];

    mat = m_matView * mat;

    m_mv00 = mat[0][0];
    m_mv01 = mat[0][1];
    m_mv02 = mat[0][2];
    m_mv03 = mat[0][3];

    m_mv10 = mat[1][0];
    m_mv11 = mat[1][1];
    m_mv12 = mat[1][2];
    m_mv13 = mat[1][3];

    m_mv20 = mat[2][0];
    m_mv21 = mat[2][1];
    m_mv22 = mat[2][2];
    m_mv23 = mat[2][3];

    m_mv30 = mat[3][0];
    m_mv31 = mat[3][1];
    m_mv32 = mat[3][2];
    m_mv33 = mat[3][3];

    UpdateData( FALSE );
}

void SceneGraphDlg::DDX_Text_Formatted(CDataExchange* pDX, int nIDC, float& value, LPCTSTR lpszOutFormat, LPCTSTR lpszInFormat)
{
    CString temp;
    if ( !pDX->m_bSaveAndValidate )
    {
        temp.Format(lpszOutFormat, value);
    }

    DDX_Text(pDX, nIDC, temp);

    if ( pDX->m_bSaveAndValidate )
    {
        const wchar_t* pBuff = temp.GetBuffer();
        swscanf(pBuff, lpszInFormat, &value);
    }
}

void SceneGraphDlg::OnBnClickedOk()
{
    CDialogEx::OnOK();
}

HBRUSH SceneGraphDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
    HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

    switch (nCtlColor)
    {
    case CTLCOLOR_EDIT:
    case CTLCOLOR_MSGBOX:
        {
            int crtlId = pWnd->GetDlgCtrlID();
            // Left (X) axis
            if ( crtlId == IDC_MV00 || crtlId == IDC_MV01 || crtlId == IDC_MV02
                || crtlId == IDC_M00 || crtlId == IDC_M01 || crtlId == IDC_M02 )
            {
                pDC->SetTextColor( RGB(128, 0, 0) );
            }
            // Up (Y) axis
            else if ( crtlId == IDC_MV10 || crtlId == IDC_MV11 || crtlId == IDC_MV12
                || crtlId == IDC_M10 || crtlId == IDC_M11 || crtlId == IDC_M12 )
            {
                pDC->SetTextColor( RGB(0, 128, 0) );
            }
            // Forward (Z) axis
            else if ( crtlId == IDC_MV20 || crtlId == IDC_MV21 || crtlId == IDC_MV22
                || crtlId == IDC_M20 || crtlId == IDC_M21 || crtlId == IDC_M22 )
            {
                pDC->SetTextColor( RGB(0, 0, 128) );
            }
            break;
        }
    }
    return hbr;
}
