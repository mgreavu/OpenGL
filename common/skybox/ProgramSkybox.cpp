#include "stdafx.h"
#include "ProgramSkybox.h"
#include <glm/gtc/type_ptr.hpp>


ProgramSkybox::ProgramSkybox()
{
}

ProgramSkybox::~ProgramSkybox()
{
}

bool ProgramSkybox::Init()
{
    if (!GlslProgram::Init())
    {
        return false;
    }

    if (!AddShader(GL_VERTEX_SHADER, "skybox.vs"))
    {
        return false;
    }

    if (!AddShader(GL_FRAGMENT_SHADER, "skybox.fs"))
    {
        return false;
    }

    if (!Finalize())
    {
        return false;
    }

    m_MVPLocation = GetUniformLocation("gMVP");
    m_samplerLocation = GetUniformLocation("cubeMap");

    if (m_MVPLocation == INVALID_UNIFORM_LOCATION ||
        m_samplerLocation == INVALID_UNIFORM_LOCATION)
    {
        return false;
    }

    return true;
}

void ProgramSkybox::SetMVP(const glm::mat4& MVP)
{
    glUniformMatrix4fv(m_MVPLocation, 1, GL_FALSE, glm::value_ptr(MVP));
}

void ProgramSkybox::SetTextureUnit(unsigned int textureUnit)
{
    glUniform1i(m_samplerLocation, textureUnit);
}
