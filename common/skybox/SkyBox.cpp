#include "stdafx.h"
#include "SkyBox.h"
#include <FreeImage.h>
#include <vector>
#include <string>

using std::string;
using std::vector;

GLfloat skyboxVertices[] = {
    // Positions
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f,  1.0f
};


SkyBox::SkyBox()
{
}

SkyBox::~SkyBox()
{
    glDeleteTextures(1, &m_texObj);
    glDeleteBuffers(1, &m_vboId);
}

bool SkyBox::Load( const char* path )
{
    if ( !( path && m_program.Init() ) )
    {
        return false;
    }

    string folder(path);
    vector<string> faces;
    faces.reserve( 6 );

    faces.push_back( folder + string("right.jpg") );
    faces.push_back( folder + string("left.jpg") );
    faces.push_back( folder + string("top.jpg") );
    faces.push_back( folder + string("bottom.jpg") );
    faces.push_back( folder + string("back.jpg") );
    faces.push_back( folder + string("front.jpg") );

    glGenBuffers(1, &m_vboId);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);

    glGenTextures(1, &m_texObj);
    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texObj);

    for ( GLuint i=0; i < faces.size(); i++ )
    {
        FREE_IMAGE_FORMAT fif = FreeImage_GetFileType( faces.at(i).c_str(), 0 );
        if ( fif == FIF_UNKNOWN )
        {
            fif = FreeImage_GetFIFFromFilename( faces.at(i).c_str() );
        }

        FIBITMAP* pImage = FreeImage_Load( fif, faces.at(i).c_str() );
        if ( pImage )
        {
            assert( FreeImage_GetBPP(pImage) == 24 );

            FreeImage_FlipVertical( pImage );

            unsigned int width = FreeImage_GetWidth( pImage );
            unsigned int height = FreeImage_GetHeight( pImage );

            BYTE *pixels = FreeImage_GetBits( pImage );

            BYTE* pData = new BYTE[width * height * 3];

            for ( unsigned int pix = 0; pix < width * height; pix++ )  // from BGR to RGB
            {
                pData[pix * 3 + 0] = pixels[pix * 3 + 2];
                pData[pix * 3 + 1] = pixels[pix * 3 + 1];
                pData[pix * 3 + 2] = pixels[pix * 3 + 0];
            }

            FreeImage_Unload( pImage );

            glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, pData );

            delete [] pData;
            pData = NULL;
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    m_program.Enable();
    m_program.SetTextureUnit(0);

    return true;
}

void SkyBox::Render( const glm::mat4& mvp )
{
    GLint oldDepthFuncMode;
    glGetIntegerv(GL_DEPTH_FUNC, &oldDepthFuncMode);

    // Change depth function so depth test passes when values are equal to depth buffer's content
    glDepthFunc(GL_LEQUAL);

    m_program.Enable();
    m_program.SetMVP(mvp);

    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texObj);

    glDrawArrays(GL_TRIANGLES, 0, 36);

    glDepthFunc(oldDepthFuncMode);
}
