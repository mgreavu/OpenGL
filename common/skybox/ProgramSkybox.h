#pragma once
#include "common/GlslProgram.h"
#include "glm/mat4x4.hpp"

class ProgramSkybox : public GlslProgram
{
public:
    ProgramSkybox();
    virtual ~ProgramSkybox();

    bool Init();

    void SetMVP(const glm::mat4& MVP);
    void SetTextureUnit(unsigned int textureUnit);

protected:
    GLuint m_MVPLocation;
    GLuint m_samplerLocation;
};

