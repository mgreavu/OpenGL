#pragma once
#include "ProgramSkybox.h"

class SkyBox
{
public:
    SkyBox();
    ~SkyBox();

    bool Load( const char* path );
    void Render( const glm::mat4& mvp );

protected:
    GLuint m_vboId;
    GLuint m_texObj;
    ProgramSkybox m_program;
};

