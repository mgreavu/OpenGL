#include "stdafx.h"
#include "SceneNode.h"
#include "common/model3d/Model3d.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

SceneNode::SceneNode()
	: m_parent( nullptr )
	, m_pModel3d(nullptr)
	, m_transform(1.0f)
	, m_worldTransform(1.0f)
	, m_trans(1.0f)
	, m_rot(1.0f)
	, m_origin(1.0f)
	, m_scale(1.0f)
{
}

SceneNode::SceneNode( shared_ptr<Model3d> pModel )
	: m_parent(nullptr)
	, m_pModel3d( pModel )
	, m_transform(1.0f)
	, m_worldTransform(1.0f)
	, m_trans(1.0f)
	, m_rot(1.0f)
	, m_origin(1.0f)
	, m_scale(1.0f)
{
}

SceneNode::~SceneNode()
{
    Iter it = GetChildrenBegin();
    while ( it != GetChildrenEnd() )
    {
        delete (*it);
        it = m_children.erase( it );
    }
}

void SceneNode::AddChild( SceneNode* pNode )
{
    pNode->m_parent = this;

    m_children.push_back( pNode );
}

void SceneNode::Update()
{
    m_transform = glm::mat4(1.0f);
    m_transform = glm::translate(m_transform, m_trans);
    m_transform *= m_rot;
    m_transform = glm::translate(m_transform, glm::vec3(-m_origin.x * m_scale.x, -m_origin.y * m_scale.x, -m_origin.z * m_scale.x));

    if ( m_parent )
    {
         m_worldTransform = m_parent->m_worldTransform * m_transform;
    }
    else
    {
         m_worldTransform = m_transform;
    }

    for ( SceneNode::Iter it = GetChildrenBegin();
        it != GetChildrenEnd(); ++it )
    {
        (*it)->Update();
    }
}

void SceneNode::SetTrans( const glm::vec3& trans )
{
    m_trans = trans;
}

void SceneNode::SetRotations( float rotX, float rotY, float rotZ )
{
    m_rot = glm::eulerAngleZY( glm::radians( rotZ ), glm::radians( rotY ) );
    m_rot *= glm::eulerAngleX( glm::radians( rotX ) );
}

void SceneNode::SetOrigin( const glm::vec3& origin )
{
    m_origin = origin;
}

void SceneNode::SetScale( float scale )
{
    m_scale.x = m_scale.y = m_scale.z = scale;
}
