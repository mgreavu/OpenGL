#include "stdafx.h"
#include "GLContext11.h"

void GLContext11::Close( HWND handle )
{
    if(!m_hdc || !m_hglrc)
        return;

    // delete DC and RC
    VERIFY(::wglMakeCurrent( NULL, NULL ));
    VERIFY(::wglDeleteContext( m_hglrc ));
    VERIFY(::ReleaseDC( handle, m_hdc ));

    m_hdc = NULL;
    m_hglrc = NULL;
}

bool GLContext11::Create( HWND handle )
{
    m_hdc = ::GetDC( handle );

    if( !SetPixelFormat() )
    {
        ::ReleaseDC( handle, m_hdc ); // remove device context
        return false;
    }

    m_hglrc = ::wglCreateContext( m_hdc );

    return ( m_hglrc != NULL );
}

bool GLContext11::Select()
{
    if ( m_hglrc == ::wglGetCurrentContext() ) return true;
    return ::wglMakeCurrent( m_hdc, m_hglrc) ? true : false;
}

bool GLContext11::Unselect()
{
    return ::wglMakeCurrent( NULL, NULL) ? true : false;
}

bool GLContext11::SwapBuffers()
{
    return ::SwapBuffers( m_hdc ) ? true : false;
}

void GLContext11::SetPolygonMode( GLenum face, GLenum mode )
{
    glPolygonMode( face, mode );
}

bool GLContext11::SetPixelFormat()
{
    PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),    // size of this pfd
        1,                                // version number
        PFD_DRAW_TO_WINDOW |              // support window
        PFD_SUPPORT_OPENGL |              // support OpenGL 
        PFD_DOUBLEBUFFER,                 // double buffered
        PFD_TYPE_RGBA,                    // RGBA type
        24,                               // 24-bit color depth
        0, 0, 0, 0, 0, 0,                 // color bits ignored
        0,                                // no alpha buffer
        0,                                // shift bit ignored
        0,                                // no accumulation buffer
        0, 0, 0, 0,                       // accum bits ignored
        32,                               // 32-bit z-buffer
        0,                                // no stencil buffer
        0,                                // no auxiliary buffer
        PFD_MAIN_PLANE,                   // main layer
        0,                                // reserved
        0, 0, 0                           // layer masks ignored
    };

    // get the device context's best, available pixel format match
    int iPixelFormat = 0;
    if ( (iPixelFormat = ::ChoosePixelFormat(m_hdc, &pfd)) == 0 )
    {
        return false;
    }

    // make that match the device context's current pixel format
    return ::SetPixelFormat(m_hdc, iPixelFormat, &pfd) ? true : false;
}
