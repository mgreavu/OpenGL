#pragma once
#include <Windows.h>

class GLContext11
{
public:
    GLContext11() {}
    virtual ~GLContext11() {}

    virtual bool Create( HWND handle );
    void Close( HWND handle );

    bool Select();
    bool Unselect();

    bool SwapBuffers();

    void SetPolygonMode( GLenum face, GLenum mode );

    HDC GetDC() const { return m_hdc; }
    HGLRC GetRC() const { return m_hglrc; }

protected:
    HDC   m_hdc;   // handle to device context
    HGLRC m_hglrc; // handle to OpenGL rendering context

private:
    bool SetPixelFormat();
};
