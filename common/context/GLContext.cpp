#include "stdafx.h"
#include "GLContext.h"

bool GLContext::Create( HWND handle )
{
    if ( !(GLContext11::Create( handle ) && ::wglMakeCurrent( m_hdc, m_hglrc )) )
    {
        return false;
    }

/*
    GLEW has a problem with core contexts. It calls glGetString(GL_EXTENSIONS), which causes GL_INVALID_ENUM on GL 3.2+ core context
    as soon as glewInit() is called. It also doesn't fetch the function pointers. The solution is for GLEW to use glGetStringi instead.
    The current version of GLEW is 1.11.0 but they still haven't corrected it.
*/
    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        dbg("GLContext() --> GLEW is not initialized, err = %d\r\n", err);
    }

    int attribs[] =
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
        WGL_CONTEXT_MINOR_VERSION_ARB, 3,
        WGL_CONTEXT_FLAGS_ARB        , 0,                                         // WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB
        WGL_CONTEXT_PROFILE_MASK_ARB , WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB, // WGL_CONTEXT_CORE_PROFILE_BIT_ARB
        0
    };

    HGLRC tempContext = m_hglrc;
    if (wglewIsSupported("WGL_ARB_create_context") == 1)
    {
        m_hglrc = wglCreateContextAttribsARB(m_hdc, 0, attribs);

        ::wglMakeCurrent(NULL, NULL);
        ::wglDeleteContext(tempContext);

        ::wglMakeCurrent(m_hdc, m_hglrc);
        dbg("GLContext() --> glError() says %d\r\n", glGetError());

        GLenum err = glewInit();
        if (err == GLEW_OK)
        {
#ifdef _DEBUG
            //Checking GL version
            const GLubyte *GLVersion = glGetString(GL_VERSION);
            const GLubyte *GLSLVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);

            //Or better yet, use the GL3 way to get the version number
            int OpenGLVersion[2];
            glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
            glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);
            dbg("GLContext() --> OpenGL %d.%d\r\n", OpenGLVersion[0], OpenGLVersion[1]);
#endif
            ::wglMakeCurrent(NULL, NULL);   // context Ok, let it be unselected by default
        }
        else
        {
            dbg("GLContext() --> GLEW was not reinitialized with the second context, err = %d\r\n", err);
            ::wglMakeCurrent(NULL, NULL);
            VERIFY(::wglDeleteContext(m_hglrc));
            m_hglrc = NULL;
        }
    }
    else //It's not possible to make a GL 3.x context
    {
        ::wglMakeCurrent(NULL, NULL);
        VERIFY(::wglDeleteContext(m_hglrc));
        m_hglrc = NULL;
    }

    return ( m_hglrc != NULL );
}
