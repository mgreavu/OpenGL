#pragma once
#include "GLContext11.h"

class GLContext : public GLContext11
{
public:
    GLContext() {}
    ~GLContext() {}

    bool Create( HWND handle );
};
