#pragma once
#include "RenderFlags.h"
#include <memory>

using std::shared_ptr;

class GLContext11;
class IProjector;
class SceneNode;

class ISceneGraphRenderer
{
public:
    ISceneGraphRenderer() {}
    virtual ~ISceneGraphRenderer() {}

    virtual bool Init( GLContext11* pGLCtx, const shared_ptr<IProjector>& proj ) = 0;
    virtual void SetFlags( int flags ) = 0;
    virtual void SetCullMode( CullMode mode ) = 0;
    virtual void SetScene( const shared_ptr<SceneNode>& pScene ) = 0;
    virtual void Render() = 0;
    virtual void RemoveScene() = 0;
    virtual void CleanUp() = 0;
    virtual void Resize( int cx, int cy ) = 0;

protected:
    virtual void PrepareScene( SceneNode* pScene ) = 0;
    virtual void RenderScene( SceneNode* pScene ) = 0;
    virtual void RemoveScene( SceneNode* pScene ) = 0;
};
