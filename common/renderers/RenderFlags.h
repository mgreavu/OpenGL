#pragma once

enum RenderFlags
{
    FLAGS_NONE          = 0x00,
    FLAG_DRAW_WIREFRAME = 0x01,
    FLAG_DRAW_BBOX      = 0x02,
    FLAG_ENABLE_LIGHTS  = 0x04,
    FLAG_ENBLE_TEXTURES = 0x08
};

enum CullMode
{
    CULL_NONE = 0,
    CULL_BACK = 1,
    CULL_FRONT = 2
};
