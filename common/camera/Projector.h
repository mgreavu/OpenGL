#pragma once
#include <glm/gtc/type_precision.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

#define MOVEMENT_SPEED 0.2f

class IProjector
{
protected:
    IProjector() {}
    IProjector( float fovyAngle, float nearPlaneDist, float farPlaneDist ) {}

public:
    virtual ~IProjector() {}

    virtual void SetViewport(const glm::i32vec2& size) = 0;
    virtual const glm::i32vec2& GetViewport() const = 0;

    virtual const glm::vec3& GetCameraPosition() const = 0;
    virtual const glm::vec3& GetCameraTarget() const = 0;
    virtual const glm::vec3& GetCameraUp() const = 0;

    virtual void SetHeading( float angle ) = 0;
    virtual float GetHeading() const = 0;

    virtual void SetTilting( float angle ) = 0;
    virtual float GetTilting() const = 0;

    virtual void MoveForward( float fact = MOVEMENT_SPEED ) = 0;
    virtual void MoveBackward( float fact = MOVEMENT_SPEED ) = 0;

    virtual void StrafeRight( float fact = MOVEMENT_SPEED ) = 0;
    virtual void StrafeLeft( float fact = MOVEMENT_SPEED ) = 0;

    virtual void MoveUp( float fact = MOVEMENT_SPEED ) = 0;
    virtual void MoveDown( float fact = MOVEMENT_SPEED ) = 0;

    virtual const glm::mat4& GetModelViewMatrix() const = 0;
    virtual const glm::mat4& GetProjectionMatrix() const = 0;
    virtual const glm::mat4& GetCombinedMVPMatrix() const = 0;

    virtual bool ProjectPoint(const glm::vec3& kInPt, glm::vec2& kOutPt) const = 0;
    virtual bool UnprojectPoint(const glm::vec2& kInPt, glm::vec3& kOutPt) const = 0;

    virtual void Update() = 0;
};
