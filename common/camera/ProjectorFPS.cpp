#include "stdafx.h"
#include "ProjectorFPS.h"
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>


ProjectorFPS::ProjectorFPS()
    : IProjector()
    , m_fovy(60.0f)
    , m_near(0.1f)
    , m_far(200.0f)
    , m_Pos(0, 2.0f, 0)
    , m_LookAt(0, 2.0f, -1.0f)
    , m_Up(0.0f, 1.0f, 0.0f)
    , m_headingAngle(0)
    , m_tiltingAngle(0)
{
    m_viewingDirection = m_LookAt - m_Pos;
}

ProjectorFPS::ProjectorFPS( float fovyAngle, float nearPlaneDist, float farPlaneDist )
    : IProjector( fovyAngle, nearPlaneDist, farPlaneDist )
    , m_fovy( fovyAngle )
    , m_near( nearPlaneDist )
    , m_far( farPlaneDist )
    , m_Pos(0, 0.0f, 0)
    , m_LookAt(0, 0.0f, 0.0f)
    , m_Up(0.0f, 0.0f, 0.0f)
    , m_headingAngle(0)
    , m_tiltingAngle(0)
{
    m_viewingDirection = m_LookAt - m_Pos;
}

ProjectorFPS::~ProjectorFPS()
{
}

void ProjectorFPS::SetViewport(const glm::i32vec2& size)
{
    m_viewport = size;
    m_MatProjection = glm::perspective( glm::radians(m_fovy), (float)m_viewport.x / m_viewport.y, m_near, m_far );
}

void ProjectorFPS::SetHeading( float angle )
{
    if (angle < 0) angle += 360;
    if (angle >= 360) angle -= 360;

    m_headingAngle = angle;
}

void ProjectorFPS::SetTilting( float angle )
{
    if (angle < 0) angle += 360;
    if (angle >= 360) angle -= 360;

    m_tiltingAngle = angle;
}

void ProjectorFPS::Update()
{
    glm::vec3 yaxis(0.0f, 1.0f, 0.0f);
    glm::mat4 matRot = glm::rotate( glm::mat4(1.0f), glm::radians(m_headingAngle), yaxis );

    glm::vec3 xaxis(1.0f, 0.0f, 0.0f);
    matRot = glm::rotate( matRot, glm::radians(m_tiltingAngle), xaxis );

    m_viewingDirection = glm::mat3(matRot) * glm::vec3(0.0f, 0.0f, -1.0f);
    m_Up               = glm::mat3(matRot) * glm::vec3(0.0f, 1.0f, 0.0f);
    m_LookAt           = m_Pos + m_viewingDirection;

    m_MatModelView = glm::lookAt( m_Pos, m_LookAt, m_Up );
    m_MatMVP = m_MatProjection * m_MatModelView;
}

void ProjectorFPS::MoveUp( float fact )
{
    m_Pos += fact * m_Up;
    m_LookAt = m_Pos + m_viewingDirection;
}

void ProjectorFPS::MoveDown( float fact )
{
    m_Pos += -fact * m_Up;
    m_LookAt = m_Pos + m_viewingDirection;
}

void ProjectorFPS::MoveForward( float fact )
{
    m_Pos += -fact * m_viewingDirection;  // vector addition
    m_LookAt = m_Pos + m_viewingDirection;
}

void ProjectorFPS::MoveBackward( float fact )
{
    m_Pos += fact * m_viewingDirection;   // vector addition, move with a fraction of the lookAt vector
    m_LookAt = m_Pos + m_viewingDirection;
}

void ProjectorFPS::StrafeRight( float fact )
{
    glm::vec3 right = glm::cross(m_viewingDirection, m_Up);
    m_Pos += fact * right;
    m_LookAt = m_Pos + m_viewingDirection;
}

void ProjectorFPS::StrafeLeft( float fact )
{
    glm::vec3 right = glm::cross(m_viewingDirection, m_Up);
    m_Pos += -fact * right;
    m_LookAt = m_Pos + m_viewingDirection;
}

bool ProjectorFPS::ProjectPoint(const glm::vec3& kInPt, glm::vec2& kOutPt) const
{
    return false;
}

bool ProjectorFPS::UnprojectPoint(const glm::vec2& kInPt, glm::vec3& kOutPt) const
{
    return false;
}
