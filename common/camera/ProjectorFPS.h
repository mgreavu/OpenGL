#pragma once
#include "Projector.h"

class ProjectorFPS : public IProjector
{
public:
    ProjectorFPS();
    ProjectorFPS( float fovyAngle, float nearPlaneDist, float farPlaneDist );
    ~ProjectorFPS();

    void SetViewport(const glm::i32vec2& size);
    const glm::i32vec2& GetViewport() const { return m_viewport; }

    const glm::vec3& GetCameraPosition() const { return m_Pos; }
    const glm::vec3& GetCameraTarget() const { return m_LookAt; }
    const glm::vec3& GetCameraUp() const { return m_Up; }

    virtual void SetHeading( float angle );
    float GetHeading() const { return m_headingAngle; }

    void SetTilting( float angle );
    float GetTilting() const { return m_tiltingAngle; }

    void MoveForward( float fact = MOVEMENT_SPEED );
    void MoveBackward( float fact = MOVEMENT_SPEED );

    void StrafeRight( float fact = MOVEMENT_SPEED );
    void StrafeLeft( float fact = MOVEMENT_SPEED );

    void MoveUp( float fact = MOVEMENT_SPEED );
    void MoveDown( float fact = MOVEMENT_SPEED );

    const glm::mat4& GetModelViewMatrix() const { return m_MatModelView; }
    const glm::mat4& GetProjectionMatrix() const { return m_MatProjection; }
    const glm::mat4& GetCombinedMVPMatrix() const { return m_MatMVP; }

    bool ProjectPoint(const glm::vec3& kInPt, glm::vec2& kOutPt) const;
    bool UnprojectPoint(const glm::vec2& kInPt, glm::vec3& kOutPt) const;

    void Update();

protected:
    float m_fovy;
    float m_near;
    float m_far;

    glm::i32vec2 m_viewport;

    glm::vec3 m_Pos;
    glm::vec3 m_LookAt;
    glm::vec3 m_Up;

    glm::vec3 m_viewingDirection;

    float m_headingAngle;
    float m_tiltingAngle;

/*
    In 3D graphics, you need to worry about several vector spaces:
    Model space      - these are usually the coordinates you specify to OpenGL
    World space      - coordinates are specified with respect to some central point in the world.
    View space       - coordinates are specified with respect to the camera
    Projection space - everything on the screen fits in the interval [-1, +1] in each dimension.
*/

    glm::mat4 m_MatModelView;
    glm::mat4 m_MatProjection;  // view to projection space
    glm::mat4 m_MatMVP;
};
