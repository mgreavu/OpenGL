#pragma once
#include "Mesh.h"
#include <string>
#include <vector>
#include "common/BoundingBox.h"

using std::string;
using std::vector;

class Model3d
{
public:
    typedef vector<Mesh>::const_iterator ConstIter;
    typedef vector<Mesh>::iterator Iter;

    Model3d()
    {}

    ~Model3d()
    {}

    ConstIter GetMeshesBegin() const
    {
        return m_geometries.begin();
    }

    ConstIter GetMeshesEnd() const
    {
        return m_geometries.end();
    }

    Iter GetMeshesBegin()
    {
        return m_geometries.begin();
    }

    Iter GetMeshesEnd()
    {
        return m_geometries.end();
    }

    const string& GetName() const { return m_name; }
    const BoundingBox& GetBBox() const { return m_bbox; }

    bool Init( const string& name, const BoundingBox& bbox, vector<Mesh>&& geometries )
    {
        m_name = name;
        m_bbox = bbox;
        m_geometries = geometries;
        return true;
    }

private:
    string       m_name;
    vector<Mesh> m_geometries;
    BoundingBox  m_bbox;
};
