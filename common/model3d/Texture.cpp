#include "stdafx.h"
#include "Texture.h"
#include <FreeImage.h>

Texture::Texture( GLenum texTarget )
    : m_texTarget( texTarget )
    , m_width( 0 )
    , m_height( 0 )
    , m_bHasAlpha ( false )
    , m_bReady( false )
    , m_pData( NULL )
{
}

Texture::~Texture()
{
    delete [] m_pData;
}

bool Texture::Load( const string& fileName )
{
    FREE_IMAGE_FORMAT fif = FreeImage_GetFileType( fileName.c_str(), 0 );
    if ( fif == FIF_UNKNOWN )
    {
        fif = FreeImage_GetFIFFromFilename( fileName.c_str() );
    }

    FIBITMAP* pImage = FreeImage_Load( fif, fileName.c_str() );
    if ( pImage )
    {
        m_width = FreeImage_GetWidth( pImage );
        m_height = FreeImage_GetHeight( pImage );

        if (FreeImage_GetBPP(pImage) != 32)
        {
            FIBITMAP* pTempImage = pImage;
            pImage = FreeImage_ConvertTo32Bits( pTempImage ); // makes a clone
            FreeImage_Unload( pTempImage );
        }

        BYTE *pixels = FreeImage_GetBits( pImage );

        m_bHasAlpha = HasAlpha( pixels, m_width, m_height );
        dbg("Texture::Load() --> \"%s\", hasAlpha = %d\r\n", fileName.c_str(), m_bHasAlpha);

        m_pData = new BYTE[m_width * m_height * 4];

        for ( unsigned int pix = 0; pix < m_width * m_height; pix++ )  // from BGRA to RGBA
        {
            m_pData[pix * 4 + 0] = pixels[pix * 4 + 2];
            m_pData[pix * 4 + 1] = pixels[pix * 4 + 1];
            m_pData[pix * 4 + 2] = pixels[pix * 4 + 0];
            m_pData[pix * 4 + 3] = pixels[pix * 4 + 3];
        }

        FreeImage_Unload( pImage );
        return true;
    }

    dbg("Texture::Load() --> FAILED: \"%s\"\r\n", fileName.c_str());
    return false;
}

bool Texture::HasAlpha( const byte* pData, size_t width, size_t height ) const
{
    int stepX = 8, stepY = 8;
    if ( width < 8 ) { stepX = width; }
    if ( height < 8 ) { stepY = height; }

    const int strideX = width / stepX, strideY = height / stepY;

    int alphaCount = 0;
    for (size_t w=0; w<width; w+=strideX)
    {
        for (size_t h=0; h<height; h+=strideY)
        {
            if (pData[w*h + 3] != 0xFF)
            {
                ++alphaCount;
            }
        }
    }
    return (alphaCount > 0.1f*stepX*stepY);
}

const void* Texture::GetData( unsigned int& width, unsigned int& height ) const
{
    width = m_width;
    height = m_height;
    return m_pData;
}
