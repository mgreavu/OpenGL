#pragma once
#include "Material.h"
#include "common/Vertex.h"
#include "common/BoundingBox.h"
#include <vector>
#include <memory>

using std::vector;
using std::shared_ptr;


struct Mesh
{
    enum { INVALID_MATERIAL = 0xFFFFFFFF };

    Mesh()
        : m_VB( 0 )
        , m_IB( 0 )
        , m_MaterialIndex( INVALID_MATERIAL )
        , m_pMaterial( NULL )
    {}

    ~Mesh() {}

    GLuint m_VB;
    GLuint m_IB;

    vector<Vertex>       m_Vertices;
    vector<unsigned int> m_Indices;

    unsigned int         m_MaterialIndex;
    shared_ptr<Material> m_pMaterial;

    BoundingBox m_bbox; // the bounding box of this mesh
};
