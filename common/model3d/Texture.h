#pragma once
#include <gl/GL.h>
#include <string>

using std::string;

class Texture
{
public:
    Texture(GLenum texTarget );
    ~Texture();

    bool Load( const string& fileName );

    bool IsReady() const { return m_bReady; }
    void SetReady( bool bReady ) { m_bReady = bReady; }

    void SetTexObj( GLuint texObj ) { m_texObj = texObj; }
    GLuint GetTexObj() const { return m_texObj; }
    GLenum GetTexTarget() const { return m_texTarget; }

    const void* GetData( size_t& width, size_t& height ) const;
    bool HasAlpha() const { return m_bHasAlpha; }

private:
    bool HasAlpha( const byte* pData, unsigned int width, unsigned int height ) const;

    GLenum m_texTarget;
    GLuint m_texObj;
    bool m_bReady;

    unsigned int m_width;
    unsigned int m_height;
    bool   m_bHasAlpha;
    BYTE*  m_pData;
};
