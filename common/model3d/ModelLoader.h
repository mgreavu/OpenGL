#pragma once
#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/matrix4x4.h>
#include <glm/mat4x4.hpp>
#include <vector>
#include <memory>
#include <map>
#include "Mesh.h"
#include "common/BoundingBox.h"

using std::vector;
using std::map;
using std::shared_ptr;

struct aiScene;
struct aiNode;
struct aiMesh;

struct Material;
class Texture;
class Model3d;

class ModelLoader
{
public:
    ModelLoader();
    ~ModelLoader();

    shared_ptr<Model3d> Load( const string& name, const string& fileName );

#ifdef _DEBUG
    struct ModelStats
    {
        unsigned int m_countMesh;
        unsigned int m_countVertex;
    };

    void GetStats( ModelStats& stats );
#endif

private:
    void InitNode( const aiNode* pNode, const aiScene* pScene, const glm::mat4& parentTransform );
    void InitMesh( unsigned int idx, const aiMesh* paiMesh, const glm::mat4& transform );
    void InitMaterials( const aiScene* pScene, const string& fileName );
    void SortByAlpha();
    void MakeBBox();
    void Clear();

    glm::mat4 AssimpMat4ToGlmMat4( const aiMatrix4x4& from ) const;

    vector<Mesh> m_geometries;
    BoundingBox  m_bbox;
    unsigned int m_meshIndex;

    Assimp::Importer m_Importer;
    unsigned int     m_postProcessFlags;

    map<string, shared_ptr<Texture>>        m_mapTex; // texture filepath to Texture pointer
    map<unsigned int, shared_ptr<Material>> m_mapMat; // material index to Material pointer
};
