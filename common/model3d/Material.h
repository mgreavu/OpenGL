#pragma once
#include "Texture.h"
#include <string>
#include <memory>

using std::string;
using std::shared_ptr;

struct Material
{
    Material()
        : m_shininess( 0 )
        , m_opacity( 1.0f )
        , m_pTexture( NULL )
    {
        memset(m_Ka, 0, sizeof(m_Ka));
        memset(m_Kd, 0, sizeof(m_Kd));
        memset(m_Ks, 0, sizeof(m_Ks));
    }

    inline bool IsAlpha() const;

    string m_name;
    float m_Ka[4];  // ambient
    float m_Kd[4];  // diffuse
    float m_Ks[4];  // specular
    float m_shininess;
    float m_opacity;

    shared_ptr<Texture> m_pTexture;
};

bool Material::IsAlpha() const
{
    bool bIs = (m_opacity < 1.0f);
    if ( !bIs && m_pTexture )
    {
        bIs = m_pTexture->HasAlpha();
    }
    return bIs;
}
