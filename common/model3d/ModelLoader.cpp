#include "stdafx.h"
#include "ModelLoader.h"
#include "Model3d.h"
#include "Texture.h"
#include "Material.h"
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags
#include <assimp/color4.h>
#include <utility>

using std::pair;

ModelLoader::ModelLoader()
    : m_meshIndex( 0 )
    , m_postProcessFlags( aiProcess_Triangulate |
                          aiProcess_FindInvalidData |
                          aiProcess_GenSmoothNormals |
                          aiProcess_JoinIdenticalVertices |
                          aiProcess_GenUVCoords |
                          aiProcess_FixInfacingNormals |
                          aiProcess_OptimizeMeshes |
                          aiProcess_FindDegenerates |
                          aiProcess_SortByPType )
{
    m_Importer.SetPropertyFloat( AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE, 80.f );
    m_Importer.SetPropertyInteger( AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_LINE | aiPrimitiveType_POINT );
}

ModelLoader::~ModelLoader()
{
    Clear();
}

void ModelLoader::Clear()
{
    m_mapMat.clear();
    m_mapTex.clear();
    // get from a valid undefined state to a valid defined state
    // following the std::move() call in Load()
    m_geometries.clear();
    m_meshIndex = 0;
}

shared_ptr<Model3d> ModelLoader::Load( const string& name, const string& fileName )
{
    shared_ptr<Model3d> spModel3d;

    const aiScene* pScene = m_Importer.ReadFile( fileName.c_str(), m_postProcessFlags );
    if ( pScene )
    {
        const aiNode* pNode = pScene->mRootNode;
        InitNode( pNode, pScene, AssimpMat4ToGlmMat4( pNode->mTransformation ) );
        InitMaterials( pScene, fileName );
        SortByAlpha();
        MakeBBox();

#ifdef _DEBUG
        ModelStats stats;
        GetStats( stats );
#endif
        spModel3d.reset( new Model3d() );
        spModel3d->Init( name, m_bbox, std::move( m_geometries ) );
    }
    else
    {
        dbg( "Load() --> Error parsing '%s': '%s'\r\n", fileName.c_str(), m_Importer.GetErrorString() );
    }

    Clear();

    return spModel3d;
}

void ModelLoader::InitNode( const aiNode* pNode, const aiScene* pScene, const glm::mat4& parentTransform )
{
    glm::mat4 transform = parentTransform * AssimpMat4ToGlmMat4( pNode->mTransformation );

    if ( pNode->mNumMeshes )
    {
        m_geometries.resize( m_geometries.size() + pNode->mNumMeshes );

        for ( unsigned int i=0; i<pNode->mNumMeshes; ++i )
        {
            const aiMesh* paiMesh = pScene->mMeshes[pNode->mMeshes[i]]; // get aiNode's aiMesh

            assert( paiMesh->mVertices && paiMesh->mNormals );
            InitMesh( m_meshIndex, paiMesh, transform );
            ++m_meshIndex;
        }
    }

    for ( unsigned int i=0; i<pNode->mNumChildren; ++i )
    {
        InitNode( pNode->mChildren[i], pScene, transform );
    }
}

void ModelLoader::InitMesh( unsigned int idx, const aiMesh* paiMesh, const glm::mat4& transform )
{
    assert( idx < m_geometries.size() );
    m_geometries[idx].m_MaterialIndex = paiMesh->mMaterialIndex;

    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
    glm::vec3 min(FLT_MAX), max(-FLT_MAX);

    vector<Vertex> vertices;
    vertices.reserve( paiMesh->mNumVertices );

    for ( unsigned int i=0; i<paiMesh->mNumVertices; ++i )
    {
        const aiVector3D* pPos      = &(paiMesh->mVertices[i]);
        const aiVector3D* pNormal   = &(paiMesh->mNormals[i]);

        const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

        glm::vec3 pos = glm::vec3( transform * glm::vec4( pPos->x, pPos->y, pPos->z, 1.0f ) );
        glm::vec3 nor = glm::vec3( transform * glm::vec4( pNormal->x, pNormal->y, pNormal->z, 1.0f ) );

        if (pos.x < min.x) { min.x = pos.x; }
        if (pos.y < min.y) { min.y = pos.y; }
        if (pos.z < min.z) { min.z = pos.z; }

        if (pos.x > max.x) { max.x = pos.x; }
        if (pos.y > max.y) { max.y = pos.y; }
        if (pos.z > max.z) { max.z = pos.z; }

        m_geometries[idx].m_bbox.set( min, max );

        vertices.push_back( Vertex( pos, glm::vec2( pTexCoord->x, pTexCoord->y ), nor ) );
    }

    vector<unsigned int> indices;
    indices.reserve( paiMesh->mNumFaces * 3 );

    for ( unsigned int i=0; i<paiMesh->mNumFaces; ++i )
    {
        const aiFace& face = paiMesh->mFaces[i];

        assert( face.mNumIndices == 3 );

        indices.push_back( face.mIndices[0] );
        indices.push_back( face.mIndices[1] );
        indices.push_back( face.mIndices[2] );
    }

    assert( vertices.size() <= 65536 );

    m_geometries[idx].m_Vertices = std::move( vertices );
    m_geometries[idx].m_Indices = std::move( indices );
}

void ModelLoader::InitMaterials( const aiScene* pScene, const string& fileName )
{
    // Extract the directory part from the file name
    string folderPath;
    string::size_type slashIndex = fileName.find_last_of("\\");

    if (slashIndex == string::npos)
    {
        folderPath = ".";
    }
    else if (slashIndex == 0)
    {
        folderPath = "\\";
    }
    else
    {
        folderPath = fileName.substr(0, slashIndex);
    }

    for ( unsigned int idxMat = 0; idxMat < pScene->mNumMaterials; idxMat++ )
    {
        const aiMaterial* pMaterial = pScene->mMaterials[idxMat];

        shared_ptr<Material> pMat( new Material() );

        aiString name;
        aiColor4D color;

        if ( pMaterial->Get( AI_MATKEY_NAME, name ) == AI_SUCCESS )
        {
            pMat->m_name = name.C_Str();
        }

        if ( aiGetMaterialColor( pMaterial, AI_MATKEY_COLOR_AMBIENT, &color ) == AI_SUCCESS )
        {
            pMat->m_Ka[0] = color.r;
            pMat->m_Ka[1] = color.g;
            pMat->m_Ka[2] = color.b;
            pMat->m_Ka[3] = color.a;
        }

        if ( aiGetMaterialColor( pMaterial, AI_MATKEY_COLOR_DIFFUSE, &color ) == AI_SUCCESS )
        {
            pMat->m_Kd[0] = color.r;
            pMat->m_Kd[1] = color.g;
            pMat->m_Kd[2] = color.b;
            pMat->m_Kd[3] = color.a;
        }

        if ( aiGetMaterialColor( pMaterial, AI_MATKEY_COLOR_SPECULAR, &color ) == AI_SUCCESS )
        {
            pMat->m_Ks[0] = color.r;
            pMat->m_Ks[1] = color.g;
            pMat->m_Ks[2] = color.b;
            pMat->m_Ks[3] = color.a;
        }

        float val;
        if ( pMaterial->Get( AI_MATKEY_SHININESS, val) == AI_SUCCESS )
        {
            pMat->m_shininess = val;
//          at some point in Assimp's past you would empirically do this: pMat->m_shininess = value / 4.0f;
        }

        if ( pMaterial->Get( AI_MATKEY_SHININESS_STRENGTH, val) == AI_SUCCESS )
        {
            pMat->m_shininess = pMat->m_shininess * val;
            if (pMat->m_shininess >= 128.0f) // 128 is the maximum exponent as per the Gl spec
            {
                pMat->m_shininess = 128.0f;
            }
        }

        if ( pMaterial->Get( AI_MATKEY_OPACITY, val) == AI_SUCCESS )
        {
            pMat->m_opacity = val;
            pMat->m_Kd[3] *= pMat->m_opacity;
        }

        if ( pMaterial->GetTextureCount( aiTextureType_DIFFUSE ) > 0 )
        {
            aiString textureFileName;
            if ( pMaterial->GetTexture( aiTextureType_DIFFUSE, 0, &textureFileName, NULL, NULL, NULL, NULL, NULL ) == AI_SUCCESS )
            {
                string texturePath = folderPath + "\\" + textureFileName.data;
                map<string, shared_ptr<Texture>>::const_iterator it = m_mapTex.find( texturePath );
                if ( it == m_mapTex.end() )
                {
                    shared_ptr<Texture> pTex( new Texture(GL_TEXTURE_2D) );
                    if ( !pTex->Load( texturePath.c_str() ) )
                    {
                        pTex.reset();
                    }

                    m_mapTex.insert( pair<string, shared_ptr<Texture>>( texturePath, pTex ) );
                    pMat->m_pTexture = pTex;
                }
                else // already allocated
                {
                    pMat->m_pTexture = (*it).second;
                }
            }
        }

        m_mapMat.insert( pair<unsigned int, shared_ptr<Material>>(idxMat, pMat) );
    }   // end materials loop

    vector<Mesh>::iterator itMesh = m_geometries.begin();
    for ( ; itMesh != m_geometries.end(); ++itMesh )
    {
        map<unsigned int, shared_ptr<Material>>::const_iterator it = m_mapMat.find( (*itMesh).m_MaterialIndex );
        if ( it != m_mapMat.end() )
        {
            (*itMesh).m_pMaterial = it->second;
        }
    }
}

void ModelLoader::SortByAlpha()
{
    struct MaterialComparer
    {
        bool operator() ( const Mesh& m1, const Mesh& m2 )
        {
            return ( !m1.m_pMaterial->IsAlpha() && m2.m_pMaterial->IsAlpha() );
        }
    } alphaComparer;

    std::sort( m_geometries.begin(), m_geometries.end(), alphaComparer );
}

void ModelLoader::MakeBBox()
{
    glm::vec3 min(FLT_MAX), max(-FLT_MAX);

    vector<Mesh>::const_iterator cit = m_geometries.begin();
    for ( ; cit != m_geometries.end(); ++cit )
    {
        if ((*cit).m_bbox._min.x < min.x) { min.x = (*cit).m_bbox._min.x; }
        if ((*cit).m_bbox._min.y < min.y) { min.y = (*cit).m_bbox._min.y; }
        if ((*cit).m_bbox._min.z < min.z) { min.z = (*cit).m_bbox._min.z; }

        if ((*cit).m_bbox._max.x > max.x) { max.x = (*cit).m_bbox._max.x; }
        if ((*cit).m_bbox._max.y > max.y) { max.y = (*cit).m_bbox._max.y; }
        if ((*cit).m_bbox._max.z > max.z) { max.z = (*cit).m_bbox._max.z; }
    }
    m_bbox.set( min, max );
}

// OpenGL and glm use column major, Assimp uses row major
glm::mat4 ModelLoader::AssimpMat4ToGlmMat4( const aiMatrix4x4& from ) const
{
    glm::mat4 to;

    to[0][0] = from.a1; to[1][0] = from.a2; to[2][0] = from.a3; to[3][0] = from.a4;
    to[0][1] = from.b1; to[1][1] = from.b2; to[2][1] = from.b3; to[3][1] = from.b4;
    to[0][2] = from.c1; to[1][2] = from.c2; to[2][2] = from.c3; to[3][2] = from.c4;
    to[0][3] = from.d1; to[1][3] = from.d2; to[2][3] = from.d3; to[3][3] = from.d4;

    return to;
}

#ifdef _DEBUG
void ModelLoader::GetStats( ModelStats& stats )
{
    memset( &stats, 0, sizeof(ModelStats) );

    stats.m_countMesh = m_geometries.size();

    vector<Mesh>::const_iterator cit = m_geometries.begin();
    for ( ; cit != m_geometries.end(); ++cit )
    {
        stats.m_countVertex += (*cit).m_Vertices.size();
    }
    dbg( "MeshCount = %d, VertexCount = %d\r\n", stats.m_countMesh, stats.m_countVertex );
}
#endif
