#pragma once
#include <vector>
#include "Vertex.h"
#include <glm/gtc/matrix_transform.hpp>

using std::vector;

struct Grid
{
    Grid()
    {
        m_vertices.reserve(2 * (4 * HALF_SIDE + 2));

        // axes through origin
        m_vertices.push_back(Vertex(glm::vec3(-HALF_SIDE, 0, 0)));
        m_vertices.push_back(Vertex(glm::vec3(HALF_SIDE, 0, 0)));

        m_vertices.push_back(Vertex(glm::vec3(0, 0, -HALF_SIDE)));
        m_vertices.push_back(Vertex(glm::vec3(0, 0, HALF_SIDE)));

        // rows
        for (int i=-HALF_SIDE; i<=HALF_SIDE; i++)
        {
            if (i == 0) continue;
            m_vertices.push_back(Vertex(glm::vec3(-HALF_SIDE, 0, i)));
            m_vertices.push_back(Vertex(glm::vec3(HALF_SIDE, 0, i)));
        }
        // cols
        for (int i=-HALF_SIDE; i<=HALF_SIDE; i++)
        {
            if (i == 0) continue;
            m_vertices.push_back(Vertex(glm::vec3(i, 0, -HALF_SIDE)));
            m_vertices.push_back(Vertex(glm::vec3(i, 0, HALF_SIDE)));
        }
    }

    void Rotate()
    {
        glm::mat3 matRot = glm::mat3( glm::rotate( glm::mat4(), glm::radians(90.0f), glm::vec3(1.0f, 0, 0) ) );
        for( vector<Vertex>::iterator it = m_vertices.begin();
            it != m_vertices.end(); ++it )
        {
            *it = matRot * (*it).m_pos;
        }
    }

    static const int HALF_SIDE = 25;
    vector<Vertex> m_vertices;
};
