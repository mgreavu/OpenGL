#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <memory>

using std::vector;
using std::shared_ptr;

class Model3d;

class SceneNode
{
public:
    typedef vector<SceneNode*>::const_iterator ConstIter;
    typedef vector<SceneNode*>::iterator Iter;

    SceneNode();
    SceneNode( shared_ptr<Model3d> pModel );
    virtual ~SceneNode();

    const glm::mat4& GetTransform() const { return m_transform; }
    const glm::mat4& GetWorldTransform() const { return m_worldTransform; }
    const glm::vec3& GetScale() const { return m_scale; }

    void SetTrans( const glm::vec3& trans );
    void SetRotations( float rotX, float rotY, float rotZ );
    void SetOrigin( const glm::vec3& origin );
    void SetScale( float scale );

    Model3d* GetModel() { return m_pModel3d.get(); }
    const Model3d* GetModel() const { return m_pModel3d.get(); }
    void SetModel( shared_ptr<Model3d> pModel ) { m_pModel3d = pModel; }

    void AddChild( SceneNode* pNode );

    ConstIter GetChildrenBegin() const
    {
        return m_children.begin();
    }

    ConstIter GetChildrenEnd() const
    {
        return m_children.end();
    }

    Iter GetChildrenBegin()
    {
        return m_children.begin();
    }

    Iter GetChildrenEnd()
    {
        return m_children.end();
    }

    void Update();

protected:
    SceneNode* m_parent;
    vector<SceneNode*> m_children;

    glm::mat4 m_transform;
    glm::mat4 m_worldTransform;

    glm::vec3 m_trans;
    glm::mat4 m_rot;
    glm::vec3 m_origin;
    glm::vec3 m_scale;

    shared_ptr<Model3d> m_pModel3d;
 };
