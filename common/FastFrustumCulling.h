#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/geometric.hpp>
#include "BoundingBox.h"

#define FFC_CULLED       0          //!< All points are outside of at least one plane
#define FFC_NOT_CULLED   1          //!< All points are inside all planes
#define FFC_INTERSECTS   2          //!< Points cross at least one plane.
                                    //!  If more than one plane, crossing may occur
                                    //!  in or out of volume.

//-----------------------------------------------------------------------------
//! @brief Fast Frustum Culling class
//!
//! Performs in/out culling checks against the viewing planes of a given
//! combined ModelView Projection matrix.
//!
//! Can test:
//!     Points
//!     Spheres
//!     Object Bounding Box (not necessarily rectangular, arbitrary orientation)
//!     Axis-Aligned Bounding Box (rectangular prism, XYZ axis aligned)
//-----------------------------------------------------------------------------
class FastFrustumCulling
{
public:
    FastFrustumCulling(void);
    ~FastFrustumCulling(void);

    //-----------------------------------------------------------------------------
    //! @brief Loads culling planes from MVP matrix
    //!
    //! This function extracts viewing plane vectors from a combined model-view
    //! projection matrix. The following paper demonstrates how matrix-vector
    //! multiplications along with solving for inequalities can extract these
    //! vectors.
    //!     Fast Extraction of Viewing Frustum Planes from the World-View-Projection Matrix
    //!     Gil Gribb, Klaus Hartmann
    //!     http://www.cs.otago.ac.nz/postgrads/alexis/planeExtraction.pdf
    //!
    //! @param [in] aMVPMatrix      Combined model-view projection matrix.
    //!
    //! @post m_PlaneVectors has been populated with new plane vectors
    //-----------------------------------------------------------------------------
    void Load( const glm::mat4& aMVPMatrix );

    int RectPrismCulled( const BoundingBox& aRectPrism ) const;
    int SphereCulled( const glm::vec3& aPos, float aRad ) const;
    int PointCulled( const glm::vec3& aPos ) const;

private:
    enum PlaneID
    {
        firstPlane = 0,

        leftPlane = firstPlane,
        rightPlane,
        bottomPlane,
        topPlane,
        nearPlane,
        farPlane,

        planeCnt
    };

    //-----------------------------------------------------------------------------
    //! @brief Normalize a Plane Vector
    //!
    //! To get the distance of a point from a plane with a dot product, the plane
    //! vector needs to be normalized.
    //!
    //! @see GetDistance()
    //!
    //! @param [in,out] aPlane  A 4d vector representing a plane to normalize
    //-----------------------------------------------------------------------------
    void NormalizePlaneVector( glm::vec4& aPlane );

    inline float GetDistance( const glm::vec4& aPlane, const glm::vec3& aPoint ) const;

    glm::vec4 m_PlaneVectors[planeCnt];    //!< Normalized vectors defining planes
};

//-----------------------------------------------------------------------------
//! @brief Get distance of a point from a plane
//!
//! Given a normalized plane vector, a dot product with a 3d point can give
//! the distance of the point from the plane.
//! Negative distances are behind the plane.
//!
//! @see Load()
//!
//! @param [in] aPlane  A normalized plane vector
//! @param [in] aPoint  A 3d point
//!
//! @return Distance from the plane. Negative values are behind the plane.
//-----------------------------------------------------------------------------
float FastFrustumCulling::GetDistance( const glm::vec4& aPlane, const glm::vec3& aPoint ) const
{
    glm::vec4 workPoint( aPoint.x, aPoint.y, aPoint.z, 1.0f );
    return glm::dot( aPlane, workPoint );
}

