#pragma once
#include <windows.h>

class MEvent
{
public:
    MEvent()
        : m_hEvent( NULL )
        , m_bManualReset( TRUE )
    {
        m_hEvent = ::CreateEvent( NULL, m_bManualReset, FALSE, NULL );
    }

    ~MEvent()
    {
        if ( m_hEvent )
        {
            ::CloseHandle( m_hEvent );
            m_hEvent = NULL;
        }
    }

    void Set() { ::SetEvent( m_hEvent ); }
    void Reset() { ::ResetEvent( m_hEvent ); }

    bool Wait( DWORD dwWaitTime )
    {
        if( ::WaitForSingleObject( m_hEvent, dwWaitTime ) != WAIT_OBJECT_0 )
        {
            return false;
        }
        return true;
    }

private:
    HANDLE m_hEvent;
    BOOL   m_bManualReset;

    MEvent(const MEvent&);
    MEvent& operator=(const MEvent&);
};
