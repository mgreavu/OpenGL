#pragma once

template<typename M>
class CScopedLock
{
public:
    explicit CScopedLock(M* m)
        : _pM(m)
    { _pM->Lock(); }

    ~CScopedLock(void)
    { _pM->Unlock(); }

    void acquire()
    { _pM->Lock(); }

    void release()
    { _pM->Unlock(); }

private:
    M* _pM;
};
