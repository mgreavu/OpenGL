#pragma once
#include <windows.h>

class CCS
{
public:
    CCS() { ::InitializeCriticalSection(&_cs); }
    ~CCS() { ::DeleteCriticalSection(&_cs); }
    void Lock() { ::EnterCriticalSection(&_cs); }
    void Unlock() { ::LeaveCriticalSection(&_cs); }

private:
    CRITICAL_SECTION _cs;

    CCS(const CCS&);
    CCS& operator=(const CCS&);
};
