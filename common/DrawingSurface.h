#pragma once
#include "afxwin.h"
#include "common/renderers/RenderFlags.h"
#include <memory>
#include <glm/vec2.hpp>

using std::shared_ptr;

class GLContext11;
class IProjector;

class DrawingSurface : public CWnd
{
public:
    enum ContextType { CONTEXT_11, CONTEXT };

    DrawingSurface( UINT_PTR tmrId, UINT interval, ContextType ctxType );
    virtual ~DrawingSurface();

    virtual void SetFlags( int flags ) { m_flags = flags; }
    int GetFlags() const { return m_flags; }

    virtual void SetCullMode( CullMode mode ) = 0;

    void EnableRefresh( bool bEnable );

    void SetProjector( const shared_ptr<IProjector>& proj )
    {
        m_pProj = proj;
    }

protected:
    ContextType m_ctxType;
    GLContext11* m_pGLCtx;

    int      m_flags;

    bool     m_bEnable;
    UINT     m_interval;
    UINT_PTR m_tmrId;

    glm::vec2   m_PosStart;
    float       m_headingStart;
    float       m_tiltingStart;
    bool        m_bOrigin;

    shared_ptr<IProjector> m_pProj;

    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

    afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnDestroy();

    DECLARE_MESSAGE_MAP()
};

