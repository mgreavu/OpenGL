#include "stdafx.h"
#include "DrawingSurface.h"
#include "common/context/GLContext11.h"
#include "common/context/GLContext.h"
#include "common/camera/Projector.h"

DrawingSurface::DrawingSurface( UINT_PTR tmrId, UINT interval, ContextType ctxType )
    : m_ctxType( ctxType )
    , m_pGLCtx( NULL )
    , m_flags( RenderFlags::FLAGS_NONE )
    , m_bEnable ( false )
    , m_tmrId( tmrId )
    , m_interval( interval )
    , m_headingStart( 0 )
    , m_tiltingStart( 0 )
    , m_bOrigin( false )
{
     dbg("DrawingSurface()\r\n");
}

DrawingSurface::~DrawingSurface()
{
    dbg("~DrawingSurface()\r\n");
}

BEGIN_MESSAGE_MAP(DrawingSurface, CWnd)
    ON_WM_CREATE()
    ON_WM_DESTROY()
END_MESSAGE_MAP()


BOOL DrawingSurface::PreCreateWindow(CREATESTRUCT& cs)
{
/*
    Basically, this is the same thing as what the folks in Win32 land do right at the beginning of their code. They "Register the class".
    Notice the CS_OWNDC. By the way, the second to last NULL (from what MSDN tells, and its been known to lie) causes the handler for WM_ERASEBKGND
    to NOT redraw the window, effectively allowing you to not have to override WM_ERASEBKGND to support flicker-free double buffering.
    CS_OWNDC = cache and use the same device context for the entire existence of the window
*/
//  cs.lpszClass = ::AfxRegisterWndClass( CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_OWNDC, ::LoadCursor(NULL, IDC_ARROW), NULL, NULL );
    cs.lpszClass = ::AfxRegisterWndClass( CS_DBLCLKS | CS_OWNDC, ::LoadCursor(NULL, IDC_ARROW), NULL, NULL );

    return CWnd::PreCreateWindow(cs);
}

int DrawingSurface::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if ( CWnd::OnCreate( lpCreateStruct ) == -1 )
    {
        return -1;
    }

    ( m_ctxType == ContextType::CONTEXT_11 ) ? m_pGLCtx = new GLContext11() : m_pGLCtx = new GLContext();
    ASSERT( m_pGLCtx );

    if ( !m_pGLCtx->Create( m_hWnd ) )
    {
        delete m_pGLCtx;
        m_pGLCtx = NULL;
        return -1;
    }

    return 0;
}

void DrawingSurface::OnDestroy()
{
    dbg("DrawingSurface::OnDestroy()\r\n");

    if (m_pGLCtx != NULL)
    {
        m_pGLCtx->Close( m_hWnd );
        delete m_pGLCtx;
        m_pGLCtx = NULL;
    }

    CWnd::OnDestroy();
}

void DrawingSurface::EnableRefresh( bool bEnable )
{
    if ( m_bEnable == bEnable) { return; }
    bEnable ? SetTimer( m_tmrId, m_interval, NULL ) : KillTimer( m_tmrId );
    m_bEnable = bEnable;
}

// Return non-zero if the message was translated and should not be dispatched.
// 0 if the message was not translated and should be dispatched.
BOOL DrawingSurface::PreTranslateMessage(MSG* pMsg)
{
    switch ( pMsg->message )
    {
    case WM_CHAR:
        {
            switch (pMsg->wParam)
            {
            case 'a':
            case 'A':
                {
                    float angle = m_pProj->GetHeading();
                    angle += 2.0f;
                    m_pProj->SetHeading(angle);
                    break;
                }
            case 'd':
            case 'D':
                {
                    float angle = m_pProj->GetHeading();
                    angle -= 2.0f;
                    m_pProj->SetHeading(angle);
                    break;
                }
            case 'w':
            case 'W':
                {
                    float angle = m_pProj->GetTilting();
                    angle -= 2.0f;
                    m_pProj->SetTilting(angle);
                    break;
                }
            case 's':
            case 'S':
                {
                    float angle = m_pProj->GetTilting();
                    angle += 2.0f;
                    m_pProj->SetTilting(angle);
                    break;
                }
            }
            return FALSE;
        }
    case WM_KEYDOWN:
        {
            switch (pMsg->wParam)
            {
            case VK_UP:
                {
                    m_pProj->MoveBackward();
                    break;
                }
            case VK_DOWN:
                {
                    m_pProj->MoveForward();
                    break;
                }
            case VK_LEFT:
                {
                    m_pProj->StrafeLeft();
                    break;
                }
            case VK_RIGHT:
                {
                    m_pProj->StrafeRight();
                    break;
                }
            case VK_INSERT:
                {
                    m_pProj->MoveUp();
                    break;
                }
            case VK_DELETE:
                {
                    m_pProj->MoveDown();
                    break;
                }
            }
            break;
        }
    }

    return CWnd::PreTranslateMessage(pMsg);
}

LRESULT DrawingSurface::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_LBUTTONDOWN:
        {
            m_PosStart = glm::vec2( GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) );
            m_headingStart = m_pProj->GetHeading();
            m_tiltingStart = m_pProj->GetTilting();
            m_bOrigin = true;
            break;
        }
    case WM_MOUSEMOVE:
        {
            if (!m_bOrigin)
                break;

            glm::vec2 posCurrent = glm::vec2( GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) );
            glm::vec2 mouseDelta = posCurrent - m_PosStart;

            const glm::i32vec2& size = m_pProj->GetViewport();
           
            m_pProj->SetHeading( m_headingStart + 360.0f * (mouseDelta.x / size.x ));
            m_pProj->SetTilting( m_tiltingStart + 90.0f * (mouseDelta.y / size.y ));
            break;
        }
    case WM_LBUTTONUP:
        {
            m_bOrigin = false;
            break;
        }
    case WM_MOUSEWHEEL:
        {
            short fwKeys = GET_KEYSTATE_WPARAM(wParam);
            short keyShift = fwKeys & MK_SHIFT;
            short zDelta = GET_WHEEL_DELTA_WPARAM(wParam);

            (zDelta > 0) ? m_pProj->MoveForward( keyShift ? 0.05f : 0.5f ) : m_pProj->MoveBackward( keyShift ? 0.05f : 0.5f );
            break;
        }
    }

    return CWnd::WindowProc(message, wParam, lParam);
}
