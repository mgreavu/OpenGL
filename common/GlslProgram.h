#pragma once
#include <GL/glew.h>
#include <list>
#include <string>

using std::list;
using std::string;

#define GLCheckError() ( glGetError() == GL_NO_ERROR )

class GlslProgram
{
public:
    GlslProgram();
    virtual ~GlslProgram();

    virtual bool Init();
    void Enable();

    enum { INVALID_UNIFORM_LOCATION = 0xffffffff };

protected:
    bool ReadShaderSource( const char* pFileName, string& outFile );
    bool AddShader( GLenum shaderType, const char* pFilename );
    bool Finalize();
    GLint GetUniformLocation( const char* pUniformName );
    GLint GetProgramParam( GLint param );

    GLuint m_shaderProg;
    list<GLuint> m_shaders;
};
