#include "stdafx.h"
#include "Projections.h"
#include "CameraVectorsDlg.h"
#include "afxdialogex.h"


// CameraVectorsDlg dialog

IMPLEMENT_DYNAMIC(CameraVectorsDlg, CDialogEx)

CameraVectorsDlg::CameraVectorsDlg(CWnd* pParent, const glm::vec3& pos, const glm::vec3& lookAt, const glm::vec3& up)
	: CDialogEx(CameraVectorsDlg::IDD, pParent)
    , m_PosX(pos.x)
    , m_PosY(pos.y)
    , m_PosZ(pos.z)
    , m_AtX(lookAt.x)
    , m_AtY(lookAt.y)
    , m_AtZ(lookAt.z)
    , m_UpX(up.x)
    , m_UpY(up.y)
    , m_UpZ(up.z)
{

}

CameraVectorsDlg::~CameraVectorsDlg()
{
}

void CameraVectorsDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Text_Formatted(pDX, IDC_POSX, m_PosX, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_POSY, m_PosY, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_POSZ, m_PosZ, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_ATX, m_AtX, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_ATY, m_AtY, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_ATZ, m_AtZ, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_UPX, m_UpX, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_UPY, m_UpY, _T("%.4lf"));
    DDX_Text_Formatted(pDX, IDC_UPZ, m_UpZ, _T("%.4lf"));
}


BEGIN_MESSAGE_MAP(CameraVectorsDlg, CDialogEx)
    ON_BN_CLICKED(IDOK, &CameraVectorsDlg::OnBnClickedOk)
END_MESSAGE_MAP()


void CameraVectorsDlg::DDX_Text_Formatted(CDataExchange* pDX, int nIDC, float& value, LPCTSTR lpszOutFormat, LPCTSTR lpszInFormat)
{
    CString temp;
    if ( !pDX->m_bSaveAndValidate )
    {
        temp.Format(lpszOutFormat, value);
    }

    DDX_Text(pDX, nIDC, temp);

    if ( pDX->m_bSaveAndValidate )
    {
        const char* pBuff = temp.GetBuffer();
        sscanf_s(pBuff, lpszInFormat, &value);
    }
}

BOOL CameraVectorsDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    UpdateData(FALSE);

    return TRUE;
}

void CameraVectorsDlg::OnBnClickedOk()
{
    UpdateData();

    CDialogEx::OnOK();
}
