//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Projections.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PROJECTIONS_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_LOCATION_DIALOG             133
#define IDD_PROJECTIONPARAM_DIALOG      134
#define IDD_CAM_VECTORS                 135
#define IDR_MENU_MAIN                   136
#define IDC_CANVAS                      1001
#define IDC_SBAR                        1002
#define IDC_CMBLOC                      1003
#define IDC_RESOLUTION                  1004
#define IDC_POSX                        1005
#define IDC_POSY                        1006
#define IDC_POSZ                        1007
#define IDC_ATX                         1008
#define IDC_ATY                         1009
#define IDC_ATZ                         1010
#define IDC_UPX                         1011
#define IDC_UPY                         1012
#define IDC_UPZ                         1013
#define ID_FILE_OPENGL3X                32786
#define ID_FILE_EXIT                    32787
#define ID_HELP_ABOUT                   32788
#define ID_TOOLS_CAMERA                 32789
#define ID_TOOLS_LOCATION               32790

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32791
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
