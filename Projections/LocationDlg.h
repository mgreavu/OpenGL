#pragma once
#include "projector/CoordType.h"

class CLocationDlg : public CDialog
{
    DECLARE_DYNAMIC(CLocationDlg)

public:
    CLocationDlg(CWnd* pParent = NULL);   // standard constructor
    virtual ~CLocationDlg();

    const GeoPoint& GetLocation() const { return m_geoPt; }

    enum { IDD = IDD_LOCATION_DIALOG };

protected:
    CString m_location;
    GeoPoint m_geoPt;

    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
    afx_msg void OnCbnDblclkCmbloc();
    DECLARE_MESSAGE_MAP()
};
