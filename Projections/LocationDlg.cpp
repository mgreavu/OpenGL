#include "stdafx.h"
#include "Projections.h"
#include "LocationDlg.h"

IMPLEMENT_DYNAMIC(CLocationDlg, CDialog)

CLocationDlg::CLocationDlg(CWnd* pParent /*=NULL*/)
    : CDialog( CLocationDlg::IDD, pParent )
    , m_location( _T("") )
{
}

CLocationDlg::~CLocationDlg()
{
}

void CLocationDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_CBString(pDX, IDC_CMBLOC, m_location);
}


BEGIN_MESSAGE_MAP(CLocationDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CLocationDlg::OnBnClickedOk)
    ON_CBN_DBLCLK(IDC_CMBLOC, &CLocationDlg::OnCbnDblclkCmbloc)
END_MESSAGE_MAP()


BOOL CLocationDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Equator intersects Prime Meridian");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Iceland");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Wuerzburg");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Cluj-Napoca");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Olathe");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("New York");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Rio de Janeiro");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Melbourne");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("New Taipei");
    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->AddString("Zanzibar");

    ((CComboBox*)(GetDlgItem(IDC_CMBLOC)))->SetCurSel(0);

    return TRUE;
}

void CLocationDlg::OnBnClickedOk()
{
    UpdateData();

    if (m_location == "Iceland")
    {
        m_geoPt.y = 64.848937f;
        m_geoPt.x = -18.350831f;
    }
    else if (m_location == "Zanzibar")
    {
        m_geoPt.y = -6.129631f;
        m_geoPt.x = 39.329223f;
    }
    else if (m_location == "New York")
    {
        m_geoPt.y = 40.749598f;
        m_geoPt.x = -74.002247f;
    }
    else if (m_location == "Wuerzburg")
    {
        m_geoPt.y = 49.791793f;
        m_geoPt.x = 9.945545f;
    }
    else if (m_location == "Cluj-Napoca")
    {
        m_geoPt.y = 46.773731f;
        m_geoPt.x = 23.600922f;
    }
    else if (m_location == "Rio de Janeiro")
    {
        m_geoPt.y = -22.905273f;
        m_geoPt.x = -43.223419f;
    }
    else if (m_location == "Melbourne")
    {
        m_geoPt.y = -37.753344f;
        m_geoPt.x = 144.968261f;
    }
    else if (m_location == "New Taipei")
    {
        m_geoPt.y = 24.881453f;
        m_geoPt.x = 121.777496f;
    }
    else if (m_location == "Olathe")
    {
        m_geoPt.y = 38.885955f;
        m_geoPt.x = -94.816875f;
    }
    else if (m_location == "Equator intersects Prime Meridian")
    {
        m_geoPt.y = 0;
        m_geoPt.x = 0;
    }

    OnOK();
}

void CLocationDlg::OnCbnDblclkCmbloc()
{
    OnBnClickedOk();
}
