#pragma once
#include "common/camera/ProjectorFPS.h"
#include "common/BoundingBox.h"
#include "CoordType.h"


class ProjectorGIS : public ProjectorFPS
{
public:
    ProjectorGIS( float fovyAngle, float nearPlaneDist, float farPlaneDist, const BoundingBox& geoArea );
    ~ProjectorGIS();

    void SetViewport(const ProjSize& size);

    void SetGeoCenter(const GeoPoint& geoCenter);
    const GeoPoint& GetGeoCenter() const { return m_GeoCenter; }

    void SetResolution(float res);
    float GetResolution() const { return m_mpp; }

    void MoveForward( float fact = MOVEMENT_SPEED );
    void MoveBackward( float fact = MOVEMENT_SPEED );

    void StrafeRight( float fact = MOVEMENT_SPEED );
    void StrafeLeft( float fact = MOVEMENT_SPEED );

    void MoveUp( float fact = MOVEMENT_SPEED );
    void MoveDown( float fact = MOVEMENT_SPEED );

    bool ProjectPoint(const GeoPoint& in3dPt, ProjPoint& out2dPt) const;
    bool UnprojectPoint(const ProjPoint& kInPt, GeoPoint& kOutPt) const;

    void Update();

    static const float MINIMUM_LATITUDE;
    static const float MAXIMUM_LATITUDE;
    static const float MINIMUM_LONGITUDE;
    static const float MAXIMUM_LONGITUDE;

protected:
    //-----------------------------------------------------------------------------
    //! @brief  Transform World coordinates to NDC space
    //!
    //! This function converts world coordinates into Normalized Device Coordinates
    //! (NDC) that represent its location within the viewing volume.
    //!
    //! Description of NDC values:
    //! Each component of the NDC value will be in the range [-1,1] for locations
    //! within the viewing volume.
    //!
    //! "X" is the position between the left and right planes.
    //! "Y" is the position between the bottom and top planes.
    //! "Z" is the position between the near and far planes.
    //!
    //! The left, bottom, and near planes are represented by "-1".
    //! The right, top, and far planes are represented by "1".
    //!
    //! @param [in] aWorldPoint (X,Y,Z) location in world space.
    //! @param [out] aNdcPoint  (X,Y,Z) location in NDC space.
    //!
    //! @return 'false' if converted location is out of the viewing volume
    //!         (i.e. All "aNdcPoint" components are not in the range of [-1,1])
    //-----------------------------------------------------------------------------
    bool WorldToNdc( const glm::vec3& aWorldPoint, glm::vec3& aNdcPoint ) const;

    //! Convert an NDC (Normalized Device Coordinate) point to a pixel coordinate
    //!
    //! @param [in] aNdcValue the NDC value to convert
    //! @param [out] aPixelValue the resulting value in pixel space
    //! @return true if the point is within the screen bounds.
    bool NdcToPixel( const ProjPoint& aNdcValue, ProjPoint& aPixelValue ) const;

    //! Convert an pixel point to an NDC (Normalized Device Coordinate) point
    //!
    //! @param [in] aPixelValue the resulting value in pixel space
    //! @param [out] aNdcValue the NDC value to convert
    //! @return true if the point is within the screen bounds.
    bool PixelToNdc( const ProjPoint & aPixelValue, ProjPoint & aNdcValue ) const;

    //-----------------------------------------------------------------------------
    //! @brief  Transform an NDC value to World coordinates
    //!
    //! This function converts a Normalized Device Coordinate (NDC) value into
    //! a location in world-space
    //!
    //! Description of NDC values:
    //! Each component of the NDC value will be in the range [-1,1] for locations
    //! within the viewing volume.
    //!
    //! "X" is the position between the left and right planes.
    //! "Y" is the position between the bottom and top planes.
    //! "Z" is the position between the near and far planes.
    //!
    //! The left, bottom, and near planes are represented by "-1".
    //! The right, top, and far planes are represented by "1".
    //!
    //! @param [in] aNdcPoint       (X,Y,Z) location in NDC space.
    //! @param [out] aWorldPoint    (X,Y,Z) location in world space.
    //!
    //! @return 'false' if "aNdcPoint" is outside of the viewing volume.
    //!         (i.e. All "aNdcPoint" components are not in the range of [-1,1])
    //-----------------------------------------------------------------------------
    bool NdcToWorld( const glm::vec3& aNdcPoint, glm::vec3& aWorldPoint ) const;

    //-----------------------------------------------------------------------------
    //! @brief  Intercept a ray with the world focal point's Z-plane
    //!
    //! This function will project a ray from the camera location through a point
    //! on the "near" plane of the viewing volume at "aNdcXYPoint".
    //! It will then compute where on the focal point's Z-plane the ray intersects.
    //!
    //! If this location is within the viewing volume, 'true' is returned and
    //! 'aWorldPoint' will contain that location.
    //!
    //! If this location is not within the viewing volume, 'false' is returned and
    //! 'aWorldPoint' contains the closest point along the ray to the Z-plane in
    //! the viewing volume.
    //!
    //! @param [in] aNdcPoint       (X,Y) location on 'near' plane in Normalized
    //!                             Device Coordinates. Z is ignored.
    //! @param [out] aWorldPoint    (X,Y,Z) location in world space in viewing volume
    //!                             closest to the focal point's Z-plane.
    //!
    //!
    //! @return 'true' the ray from camera, through 'aNdcXYPoint', intercepts
    //!         the focal point's Z-plane in the viewing volume.
    //-----------------------------------------------------------------------------
    bool InterceptWorldZPlane( const glm::vec3& aNdcPoint, glm::vec3& aWorldPoint ) const;

#ifdef DEBUG
    void DumpMat4(const glm::mat4& x);
    void DumpVec3(const glm::vec3& v);
#endif

    const BoundingBox m_GeoArea;
    GeoPoint          m_GeoCenter;
    ProjPoint         m_ProjCenter;

    float             m_mpp;    // meters per pixel
    glm::vec3         m_scale;

    float             m_Distance;   // camera distance

    glm::mat4 m_MatInvMVP;  // inverse of MVP
};
