#include "stdafx.h"
#include "ProjectorGIS.h"
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _DEBUG
#include <glm/gtx/string_cast.hpp>
#endif

#define Re 6378137  // Earth radius in meters
#define Pi 3.1415926f

const float ProjectorGIS::MINIMUM_LATITUDE  = -90.0f;
const float ProjectorGIS::MAXIMUM_LATITUDE  = 90.0f;
const float ProjectorGIS::MINIMUM_LONGITUDE = -180.0f;
const float ProjectorGIS::MAXIMUM_LONGITUDE = 180.0f;


ProjectorGIS::ProjectorGIS( float fovyAngle, float nearPlaneDist, float farPlaneDist, const BoundingBox& geoArea )
    : ProjectorFPS( fovyAngle, nearPlaneDist, farPlaneDist )
    , m_GeoArea( geoArea )
    , m_mpp( 1.0f )
    , m_scale( 1.0f, 1.0f, 1.0f )
    , m_Distance( 40.0f )
{
    m_GeoCenter = m_GeoArea.center();
}

ProjectorGIS::~ProjectorGIS()
{
    dbg("~ProjectorGIS()\r\n");
}

void ProjectorGIS::Update()
{
    // see http://www.songho.ca/opengl/gl_transform.html

    /* basis vectors construction */
    // start with orthogonal, unit vectors.
    // the eye-vector down the Z-axis and the up-vector along positive Y-axis
    m_viewingDirection.x = 0.0f;
    m_viewingDirection.y = 0.0f;
    m_viewingDirection.z = -1.0f;

    m_Up.x = 0.0f;
    m_Up.y = 1.0f;
    m_Up.z = 0.0f;

    // rotate along positive Z-axis for heading, the viewing direction won't change
    glm::mat4 matRot = glm::rotate( glm::mat4(), glm::radians(m_headingAngle), glm::vec3(0.0f, 0.0f, 1.0f) );
    m_Up = glm::mat3(matRot) * m_Up;

    // rotate about our new X-axis for tilt.
    glm::vec3 right = glm::cross( m_viewingDirection, m_Up );
    matRot = glm::rotate( glm::mat4(), glm::radians(m_tiltingAngle), right );
    m_viewingDirection = glm::mat3(matRot) * m_viewingDirection;
    m_Up               = glm::mat3(matRot) * m_Up;

    // ModelView matrix construction, replace glm::lookAt()

    // create the translation matrix
    m_MatModelView = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -m_Distance));

    // create the rotation matrix using the basis vectors
    matRot = glm::mat4();   // load identity

    matRot[0][0] = right.x;
    matRot[1][0] = right.y;
    matRot[2][0] = right.z;

    matRot[0][1] = m_Up.x;
    matRot[1][1] = m_Up.y;
    matRot[2][1] = m_Up.z;

    matRot[0][2] = -m_viewingDirection.x;
    matRot[1][2] = -m_viewingDirection.y;
    matRot[2][2] = -m_viewingDirection.z;

    m_MatModelView = m_MatModelView * matRot;
    m_MatModelView = glm::scale( m_MatModelView, m_scale );

    glm::vec3 lookAtVector = m_viewingDirection * m_Distance;
    m_Pos = m_LookAt - lookAtVector;    // calculate camera position

#if 0
    DumpVec3(m_Pos);
    DumpVec3(m_LookAt);
    DumpVec3(m_GeoCenter);
#endif

#if 0
    DumpMat4(m_MatModelView);

    glm::mat4 matView = glm::lookAt( m_Pos, m_LookAt, m_Up );
    DumpMat4(matView);
#endif

    m_MatMVP = m_MatProjection * m_MatModelView;
    m_MatInvMVP = glm::inverse(m_MatMVP);
}

void ProjectorGIS::SetGeoCenter(const GeoPoint& geoCenter)
{
    if ( m_GeoArea.contains(geoCenter) )
    {
        m_GeoCenter = geoCenter;
    }
}

void ProjectorGIS::MoveForward( float fact )
{
    glm::vec3 heading(0, 1, 0);
    glm::mat4 matRot = glm::rotate( glm::mat4(), glm::radians(m_headingAngle), glm::vec3(0, 0, 1.0f) );
    heading = glm::mat3(matRot) * heading;

    m_GeoCenter -= heading * fact;
}

void ProjectorGIS::MoveBackward( float fact )
{
    glm::vec3 heading(0, 1, 0);
    glm::mat4 matRot = glm::rotate( glm::mat4(), glm::radians(m_headingAngle), glm::vec3(0, 0, 1.0f) );
    heading = glm::mat3(matRot) * heading;

    m_GeoCenter += heading * fact;
}

void ProjectorGIS::StrafeRight( float fact )
{
    glm::vec3 heading(1.0, 0, 0);
    glm::mat4 matRot = glm::rotate( glm::mat4(), glm::radians(m_headingAngle), glm::vec3(0, 1.0f, 0) );
    heading = glm::mat3(matRot) * heading;

    m_GeoCenter += heading * fact;
}

void ProjectorGIS::StrafeLeft( float fact )
{
    glm::vec3 heading(1.0, 0, 0);
    glm::mat4 matRot = glm::rotate( glm::mat4(), glm::radians(m_headingAngle), glm::vec3(0, 1.0f, 0) );
    heading = glm::mat3(matRot) * heading;

    m_GeoCenter -= heading * fact;
}

void ProjectorGIS::MoveUp( float fact )
{
    m_Distance -= fact;
}

void ProjectorGIS::MoveDown( float fact )
{
    m_Distance += fact;
}

void ProjectorGIS::SetViewport(const ProjSize& size)
{
    ProjectorFPS::SetViewport(size);

    m_ProjCenter.x = (static_cast<float>(m_viewport.x) - 1) / 2;
    m_ProjCenter.y = (static_cast<float>(m_viewport.y) - 1) / 2;
}

void ProjectorGIS::SetResolution(float res)
{
    m_mpp = res;    // how many meters per pixel do I want
    dbg("SetResolution() --> m_mpp = %.2f mpp\r\n", m_mpp);
/*
    float metersAtEquator = 2 * Pi * Re;
    float metersPerDegreeAtEquator = metersAtEquator / 360.0f;

    m_scale.x = metersPerDegreeAtEquator / m_mpp;
    m_scale.y = metersPerDegreeAtEquator / m_mpp;
    m_scale.z = metersPerDegreeAtEquator / m_mpp;
*/
}

bool ProjectorGIS::ProjectPoint(const GeoPoint& in3dPt, ProjPoint& out2dPt) const
{
    glm::vec3 world3dPt(
    static_cast<float>(in3dPt.x - m_GeoCenter.x),
    static_cast<float>(in3dPt.y - m_GeoCenter.y),
    static_cast<float>(in3dPt.z) );

    glm::vec3 ndcPt3d;
    bool bOk = WorldToNdc( world3dPt, ndcPt3d );

    out2dPt.x = ndcPt3d.x;
    out2dPt.y = ndcPt3d.y;

    bOk = bOk && NdcToPixel(out2dPt, out2dPt);

//  dbg("xy = (%.2f, %.2f)\r\n", out2dPt.x, out2dPt.y);
    return bOk;
}

bool ProjectorGIS::UnprojectPoint(const ProjPoint& kInPt, GeoPoint& kOutPt) const
{
    ProjPoint outNdc;
    bool bOk = PixelToNdc(kInPt, outNdc);

    glm::vec3 ndcPt3d( outNdc.x, outNdc.y, 0.0f );
    glm::vec3 worldPt3d;
    bOk = bOk && InterceptWorldZPlane( ndcPt3d, worldPt3d );

    kOutPt.x = worldPt3d.x + m_GeoCenter.x;
    kOutPt.y = worldPt3d.y + m_GeoCenter.y;

    return bOk;
}

bool ProjectorGIS::WorldToNdc( const glm::vec3& aWorldPoint, glm::vec3& aNdcPoint ) const
{
    bool retVal = false;

    glm::vec4 worldPoint = glm::vec4( aWorldPoint.x, aWorldPoint.y, aWorldPoint.z, 1.0f );
    glm::vec4 ndcPoint;

    ndcPoint = m_MatMVP * worldPoint; // convert World point to NDC

//  dbg("CLIP xyzw = (%.2f, %.2f, %.2f %.2f)\r\n", ndcPoint.x, ndcPoint.y, ndcPoint.z, ndcPoint.w);
//  ASSERT( ndcPoint.w > 0);

    if ( ndcPoint.w > 0)
    {
        ndcPoint = ndcPoint / ndcPoint.w;
        aNdcPoint.x = ndcPoint.x;
        aNdcPoint.y = ndcPoint.y;
        aNdcPoint.z = ndcPoint.z;

        retVal = true;
    }

//  dbg("CLIP xyz = (%.2f, %.2f, %.2f)\r\n", aNdcPoint.x, aNdcPoint.y, aNdcPoint.z);
    return retVal;
}

//  @see glViewport()
bool ProjectorGIS::NdcToPixel( const ProjPoint& aNdcValue, ProjPoint& aPixelValue ) const
{
    bool bOk = ( aNdcValue.x <= 1.0f && aNdcValue.x >= -1.0f && aNdcValue.y <= 1.0f && aNdcValue.y >= -1.0f );

    aPixelValue.x = ( aNdcValue.x + 1.0f ) / 2.0f * static_cast<float>(m_viewport.x);
    aPixelValue.y = ( 1.0f - aNdcValue.y ) / 2.0f * static_cast<float>(m_viewport.y);

    return bOk;
}

bool ProjectorGIS::PixelToNdc( const ProjPoint & aPixelValue, ProjPoint & aNdcValue ) const // Z info lost
{
    aNdcValue.x = ( aPixelValue.x / static_cast<float>(m_viewport.x) * 2.0f - 1.0f );
    aNdcValue.y = 1.0f - ( aPixelValue.y / static_cast<float>(m_viewport.y) * 2.0f );

    return ( aNdcValue.x <= 1.0f && aNdcValue.x >= -1.0f && aNdcValue.y <= 1.0f && aNdcValue.y >= -1.0f );
}

bool ProjectorGIS::NdcToWorld( const glm::vec3& aNdcPoint, glm::vec3& aWorldPoint ) const
{
    bool retVal = false;

    // Use vec4 so we can examine the W component of the output
    glm::vec4 ndcPoint( aNdcPoint.x, aNdcPoint.y, aNdcPoint.z, 1.0f );
    glm::vec4 worldPoint;

    // Convert NDC point to World
    worldPoint = m_MatInvMVP * ndcPoint;

    // If W is more than slightly positive, the projection succeeded
    if( worldPoint.w > 0 )
    {
        worldPoint = worldPoint / worldPoint.w;
        aWorldPoint.x = worldPoint.x;
        aWorldPoint.y = worldPoint.y;
        aWorldPoint.z = worldPoint.z;

        retVal = true;
    }
    return retVal;
}

bool ProjectorGIS::InterceptWorldZPlane( const glm::vec3& aNdcPoint, glm::vec3& aWorldPoint ) const
{
    bool retVal = false;

    // Near plane conversion (ndc.z = -1)
    glm::vec3 ndcInput;
    ndcInput.x = aNdcPoint.x;
    ndcInput.y = aNdcPoint.y;
    ndcInput.z = -1;

    glm::vec3 nearLoc;
    if( ! NdcToWorld( ndcInput, nearLoc ) )
    {
        // Input error (release version bail-out)
        return retVal;
    }

    // Far plane conversion (ndc.z = 1)
    ndcInput.z = 1;

    glm::vec3 farLoc;
    retVal = NdcToWorld( ndcInput, farLoc ); // Input errors would be caught during near plane conversion.

    // ModelViewProj matrix is focused at (0,0,0).
    // See CalculateOpenGLMatrix()
    float worldZPlane = 0;

    if( nearLoc.z >= worldZPlane && farLoc.z <= worldZPlane )
    {
        // Z-Plane intercept occurs within the viewing volume
        glm::vec3 diff = farLoc - nearLoc;
        float zDistScaling = (worldZPlane - nearLoc.z) / diff.z;

        aWorldPoint.x = zDistScaling * diff.x + nearLoc.x;
        aWorldPoint.y = zDistScaling * diff.y + nearLoc.y;
        aWorldPoint.z = worldZPlane;
    }
    else
    {
        // Since tilting always moves the upper half of the world plane towards
        // the far plane (see UpdateBasisVectors()), positive aNdcPoint.y values
        // will limit to the far plane.
        if( aNdcPoint.y > 0.0f )
        {
            // Set point to far plane intercept
            aWorldPoint = farLoc;
        }
        else
        {
            // Set point to near plane intercept
            aWorldPoint = nearLoc;
        }
    }

    return retVal;
}

#ifdef _DEBUG
void ProjectorGIS::DumpMat4(const glm::mat4& x)
{
/*
    Matrix types store their values in column-major order. (glm manual section 5.13)
    The matrices are accessed as columns. You can access the columns with [] operator.
    For example matrix[3] will give the last column in mat4,
    which should be the translation part (of a ModelView matrix).
    So you can say glm::vec3 position = glm::vec3(mat[3]);
*/
    std::string out = glm::detail::format("(%.5f, %.5f, %.5f, %.5f)\r\n(%.5f, %.5f, %.5f, %.5f)\r\n(%.5f, %.5f, %.5f, %.5f)\r\n(%.5f, %.5f, %.5f, %.5f)\r\n",
        x[0][0], x[1][0], x[2][0], x[3][0],
        x[0][1], x[1][1], x[2][1], x[3][1],
        x[0][2], x[1][2], x[2][2], x[3][2],
        x[0][3], x[1][3], x[2][3], x[3][3]);

    dbg("%s\r\n", out.c_str());
}

void ProjectorGIS::DumpVec3(const glm::vec3& v)
{
    std::string out = glm::detail::format("(%.2f, %.2f, %.2f)", v.x, v.y, v.z);
    dbg("%s\r\n", out.c_str());
}
#endif
