#pragma once
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/gtc/type_precision.hpp>

typedef glm::vec3    GeoPoint; // x = longitude, y = latitude, z = height
typedef glm::vec2    ProjPoint;
typedef glm::i32vec2 ProjSize;
