#pragma once

class ProjectionParamsDlg : public CDialog
{
	DECLARE_DYNAMIC(ProjectionParamsDlg)

public:
	ProjectionParamsDlg(CWnd* pParent, float res);   // standard constructor
	virtual ~ProjectionParamsDlg();

	enum { IDD = IDD_PROJECTIONPARAM_DIALOG };

    float GetResoultion() const { return m_Res; }

protected:
    float m_Res;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
	DECLARE_MESSAGE_MAP()
};
