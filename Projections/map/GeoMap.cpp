#include "StdAfx.h"
#include "GeoMap.h"
#include <sstream>
#include <fstream>

using std::istringstream;
using std::ifstream;

GeoMap::GeoMap(const string& fileName)
    : m_fileName(fileName)
{
}

GeoMap::~GeoMap()
{
}

bool GeoMap::LoadMap()
{
    ifstream infile;
    infile.open(m_fileName.c_str());
    if ( !infile.is_open() )
    {
        return false;
    }

    string line;
    GeoPoint ptGeo;
    while( getline(infile, line) )
    {
        istringstream in(line);
        in >> ptGeo.x >> ptGeo.y >> ptGeo.z;
        dbg("lon = %f, lat = %f\r\n", ptGeo.x, ptGeo.y);
        m_veGeoPts.push_back(ptGeo);
    }
    infile.close();
    return (m_veGeoPts.size() > 0);
}

bool GeoMap::LoadMap(const BoundingBox& geoArea)
{
    ifstream infile;
    infile.open(m_fileName.c_str());
    if ( !infile.is_open() )
    {
        return false;
    }

    string line;
    GeoPoint ptGeo;
    while( getline(infile, line) )
    {
        istringstream in(line);
        in >> ptGeo.x >> ptGeo.y; ptGeo.z = 0;
        if ( geoArea.contains(ptGeo) )
        {
            m_veGeoPts.push_back(ptGeo);
        }
    }
    infile.close();
    return (m_veGeoPts.size() > 0);
}

void GeoMap::CloseMap()
{
    m_fileName.clear();
    m_veGeoPts.clear();
}
