#pragma once
#include "projector/CoordType.h"
#include "common/BoundingBox.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

class GeoMap
{
public:
    GeoMap(const string& fileName);
    ~GeoMap();

    bool LoadMap();
    bool LoadMap(const BoundingBox& geoArea);
    void CloseMap();

    void SetFileName(const string& fileName) { m_fileName = fileName; }

    const vector<GeoPoint>& GetGeoPts() const { return m_veGeoPts; }

private:
    string m_fileName;
    vector<GeoPoint> m_veGeoPts;
};
