#include "stdafx.h"
#include "Projections.h"
#include "ProjectionsDlg.h"
#include "AboutDlg.h"
#include "afxdialogex.h"
#include "map/GeoMap.h"
#include "projector/ProjectorGIS.h"
#include "surfaces/DrawingSurfaceMapGL3x.h"
#include "CameraVectorsDlg.h"
#include "LocationDlg.h"
// #include "ProjectionParamsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CProjectionsDlg::CProjectionsDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx( CProjectionsDlg::IDD, pParent )
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

    m_SBarWidths[0] = 100;
    m_SBarWidths[1] = -1;

    m_geoArea.set( GeoPoint(-180, -90, 0), GeoPoint(180, 90, 0) );
//  m_geoArea.set( GeoPoint(-20.0f, 30.0f, 0), GeoPoint(40.0f, 72.0, 0) );  // Europe
}

CProjectionsDlg::~CProjectionsDlg()
{
}

void CProjectionsDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProjectionsDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_WM_SIZE()
    ON_COMMAND(ID_FILE_OPENGL3X, &CProjectionsDlg::OnFileOpengl3x)
    ON_COMMAND(ID_FILE_EXIT, &CProjectionsDlg::OnFileExit)
    ON_COMMAND(ID_TOOLS_CAMERA, &CProjectionsDlg::OnToolsCamera)
    ON_COMMAND(ID_HELP_ABOUT, &CProjectionsDlg::OnHelpAbout)
    ON_COMMAND(ID_TOOLS_LOCATION, &CProjectionsDlg::OnToolsLocation)
END_MESSAGE_MAP()

BOOL CProjectionsDlg::PreTranslateMessage(MSG* pMsg)
{
    if (pMsg->message == WM_CHAR)
    {
        switch (pMsg->wParam)
        {
        case 'g':
        case 'G':
            {
                OnToolsLocation();
                return TRUE;
            }
        case '+':
        case '=':
            {
                float res = m_pProj->GetResolution();
                res /= 1.1f;
                m_pProj->SetResolution(res);
                break;
            }
        case '-':
            {
                float res = m_pProj->GetResolution();
                res *= 1.1f;
                m_pProj->SetResolution(res);
                break;
            }
        case 'c':
        case 'C':
            {
                OnToolsCamera();
                return TRUE;
            }
        }
        return FALSE;
    }

    return CDialogEx::PreTranslateMessage(pMsg);
}

bool CProjectionsDlg::LoadMaps()
{
    WIN32_FIND_DATA ffd;
    TCHAR szDir[MAX_PATH + 1];
    HANDLE hFind = INVALID_HANDLE_VALUE;

    ::GetModuleFileName(NULL, szDir, MAX_PATH);

    TCHAR *pfound = _tcsrchr(szDir, _T('\\'));
    if (!pfound)
        return FALSE;
    *pfound = _T('\0');

    string procPath = szDir;
    procPath += "\\data\\maps\\";

    _tcsncat_s(szDir, MAX_PATH, _T("\\data\\maps\\*.txt"), _tcslen(_T("\\data\\maps\\*.txt")));
    if ((hFind = ::FindFirstFile(szDir, &ffd)) == INVALID_HANDLE_VALUE)
        return FALSE;
    do
    {
        if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
        {
            m_veMaps.push_back(shared_ptr<GeoMap>(new GeoMap(procPath + ffd.cFileName)));
        }
    }
    while (::FindNextFile(hFind, &ffd) != 0);

    ::FindClose(hFind);

    bool bOk = true;

    vector<shared_ptr<GeoMap> >::iterator itMaps = m_veMaps.begin();
    for (; itMaps != m_veMaps.end(); ++itMaps)
    {
        if (!(*itMaps)->LoadMap(m_geoArea))
            bOk = false;
    }
    return bOk;
}

/*
BOOL CProjectionsDlg::OnTbnProjSettings(UINT cmd)
{
    ProjectionParamsDlg dlgProj(this, _pProj->GetResolution());
    if (dlgProj.DoModal() != IDOK)
        return FALSE;

    _pProj->SetResolution( dlgProj.GetResoultion() );

    return TRUE;
}
*/

BOOL CProjectionsDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    SetIcon(m_hIcon, TRUE);
    SetIcon(m_hIcon, FALSE);

    if ( !m_menu.LoadMenu(IDR_MENU_MAIN) )
    {
        AfxThrowResourceException();
    }
    SetMenu( &m_menu );

    m_statusBar.Create(WS_CHILD | WS_VISIBLE | CCS_BOTTOM | SBARS_SIZEGRIP, CRect(0,0,0,0), this, IDC_SBAR);
    CRect rcClient;
    GetClientRect(rcClient);
    m_SBarWidths[0] = static_cast<int>(rcClient.right * 0.66);
    m_statusBar.SetParts(2, m_SBarWidths);
    m_statusBar.SetText("Ready", 0, 0);

    MoveWindow( CRect(CPoint(0, 0), CPoint(1000, 600)), FALSE );

    m_pProj.reset( new ProjectorGIS( 90.0f, 0.2f, 200.0f, m_geoArea) );

    LoadMaps();

    OnFileOpengl3x();

    return TRUE;
}

void CProjectionsDlg::OnFileOpengl3x()
{
    m_menu.EnableMenuItem(ID_FILE_OPENGL3X, MF_GRAYED);

    if ( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) )
    {
        m_pDrawingSurface->DestroyWindow();
    }
#ifdef _DEBUG
    m_pDrawingSurface.reset( new DrawingSurfaceMapGL3x(50) );
#else
    m_pDrawingSurface.reset( new DrawingSurfaceMapGL3x(16) ); // higher fps
#endif
    InitDS();
}

bool CProjectionsDlg::InitDS()
{
    m_pDrawingSurface->SetProjector( m_pProj );

    CRect rcClient, rcSbar;
    m_statusBar.GetClientRect(rcSbar);
    GetClientRect(rcClient);
    rcClient.bottom -= rcSbar.Height();
    bool bOk = ( m_pDrawingSurface->Create( NULL, NULL, WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE, rcClient, this, IDC_CANVAS ) != -1);
    if ( bOk )
    {
        vector<shared_ptr<GeoMap> >::iterator itMaps = m_veMaps.begin();
        for (; itMaps != m_veMaps.end(); ++itMaps)
        {
            const vector<GeoPoint> vePts = (*itMaps)->GetGeoPts();

            IPolygonHandler* pPolyHandler = dynamic_cast<IPolygonHandler*>(m_pDrawingSurface.get());
            if ( pPolyHandler )
            {
                pPolyHandler->AddPolygon(vePts);
            }
        }
        m_pDrawingSurface->EnableRefresh( true );
        m_pDrawingSurface->SetFocus();
    }
    return bOk;
}

void CProjectionsDlg::OnSize(UINT nType, int cx, int cy)
{
    CDialogEx::OnSize(nType, cx, cy);

    CRect rcSbar;
    if (m_statusBar.GetSafeHwnd())
    {
        m_statusBar.GetClientRect(rcSbar);
        m_statusBar.MoveWindow(0, cy - rcSbar.Height(), cx, rcSbar.Height());
        m_SBarWidths[0] = static_cast<int>(0.66f * rcSbar.Width());
        m_statusBar.SetParts(2, m_SBarWidths);
    }
    if ( m_pDrawingSurface && ::IsWindow(m_pDrawingSurface->GetSafeHwnd()) )
    {
        m_pDrawingSurface->MoveWindow( 0, 0, cx, cy - rcSbar.Height() );
        m_pDrawingSurface->SetFocus();  // make sure we get WM_MOUSEWHEEL
    }
}

void CProjectionsDlg::UpdateStatusBar(float lon, float lat)
{
    CString coords;
    coords.Format( "(lon, lat) = (%.4f, %.4f)", lon, lat );
    m_statusBar.SetText(coords, 0, 0);
}

BOOL CProjectionsDlg::OnEraseBkgnd(CDC* pDC)
{
    return TRUE;
}

void CProjectionsDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

void CProjectionsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

HCURSOR CProjectionsDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void CProjectionsDlg::OnFileExit()
{
    OnCancel();
}

void CProjectionsDlg::OnToolsCamera()
{
    CameraVectorsDlg dlgCamVec( this, m_pProj->GetCameraPosition(), m_pProj->GetCameraTarget(), m_pProj->GetCameraUp() );
    dlgCamVec.DoModal();
}

void CProjectionsDlg::OnToolsLocation()
{
    CLocationDlg dlgLoc;
    if (dlgLoc.DoModal() == IDOK)
    {
        GeoPoint pt = dlgLoc.GetLocation();
        m_pProj->SetGeoCenter(pt);
    }
}

void CProjectionsDlg::OnHelpAbout()
{
    CAboutDlg dlgAbout;
    dlgAbout.DoModal();
}
