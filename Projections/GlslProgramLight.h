#pragma once
#include "common/GlslProgram.h"
#include "common/lights/DirectionalLight.h"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"


class GlslProgramLight : public GlslProgram
{
public:

    GlslProgramLight();

    virtual bool Init();

    void SetMVP(const glm::mat4& MVP);
    void SetGeoCenter( const glm::vec3& center );
    void SetDirectionalLight(const DirectionalLight& Light);

private:

    GLuint m_MVPLocation;
    GLuint m_GeoCenter;
    GLuint m_dirLightColorLocation;
    GLuint m_dirLightAmbientIntensityLocation;
};
