#pragma once
#include <vector>
#include "projector/CoordType.h"

using std::vector;

class IPolygonHandler
{
public:
    virtual void AddPolygon(const vector<GeoPoint>& veGeoPts) = 0;
    virtual void RemovePolygons() = 0;
};
