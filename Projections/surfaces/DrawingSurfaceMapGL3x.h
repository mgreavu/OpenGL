#pragma once
#include "common/DrawingSurface.h"
#include "PolygonHandler.h"
#include "projector/CoordType.h"
#include "GlslProgramLight.h"
#include <vector>
#include <map>

using std::vector;
using std::map;

class DrawingSurfaceMapGL3x : public DrawingSurface, public IPolygonHandler
{
public:
    DrawingSurfaceMapGL3x( UINT interval );
    virtual ~DrawingSurfaceMapGL3x();

    void AddPolygon(const vector<GeoPoint>& veGeoPts);
    void RemovePolygons();

    void SetCullMode( CullMode mode ) {}

protected:
    map<GLuint, vector<GeoPoint>> m_shapes;
    DirectionalLight m_lightShapes;

    GLuint           m_gridId;
    DirectionalLight m_lightGridAxes;
    DirectionalLight m_lightGrid;

    GlslProgramLight m_program;

    void RenderGrid();

    afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);

    DECLARE_MESSAGE_MAP()
};

