#include "stdafx.h"
#include "DrawingSurfaceMapGL3x.h"
#include "common/context/GLContext.h"
#include "ProjectionsDlg.h"
#include "projector/ProjectorGIS.h"
#include "common/Grid.h"
#include <utility>

using std::pair;
using std::static_pointer_cast;

DrawingSurfaceMapGL3x::DrawingSurfaceMapGL3x( UINT interval )
    : DrawingSurface( 3785, interval, DrawingSurface::CONTEXT )
{
}

DrawingSurfaceMapGL3x::~DrawingSurfaceMapGL3x()
{
    dbg("~DrawingSurfaceMapGL3x()\r\n");
}

void DrawingSurfaceMapGL3x::OnDestroy()
{
    dbg("DrawingSurfaceMapGL3x::OnDestroy()\r\n");

    if (m_pGLCtx != NULL)
    {
        if (::wglGetCurrentContext() != NULL)
        {
            glDisableVertexAttribArray(0);
            RemovePolygons();
            glDeleteBuffers(1, &m_gridId);
        }
    }

    DrawingSurface::OnDestroy();
}

BEGIN_MESSAGE_MAP(DrawingSurfaceMapGL3x, DrawingSurface)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_TIMER()
    ON_WM_DESTROY()
    ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

int DrawingSurfaceMapGL3x::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (DrawingSurface::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }

    if ( !m_pGLCtx->Select() )
    {
        return -1;
    }

    if (!m_program.Init())
    {
        return -1;
    }

//  glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    m_program.Enable();

    glEnableVertexAttribArray(0);

    m_lightShapes.m_color = glm::vec3(0.0f, 1.0f, 0.0f);
    m_lightShapes.m_ambientIntensity = 0.75f;

    // the grid
    Grid grid;
    grid.Rotate();
    glGenBuffers(1, &m_gridId);
    glBindBuffer(GL_ARRAY_BUFFER, m_gridId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * grid.m_vertices.size(), &grid.m_vertices[0], GL_STATIC_DRAW);

    m_lightGridAxes.m_color = glm::vec3(1.0f, 0.0f, 0.0f);
    m_lightGridAxes.m_ambientIntensity = 0.5f;
    m_lightGrid.m_color = glm::vec3(1.0f, 1.0f, 1.0f);
    m_lightGrid.m_ambientIntensity = 0.25f;

    return 0;
}

void DrawingSurfaceMapGL3x::OnSize(UINT nType, int cx, int cy)
{
    DrawingSurface::OnSize(nType, cx, cy);

    m_pProj->SetViewport( glm::i32vec2(cx, cy) );
    glViewport(0, 0, cx, cy);
}

void DrawingSurfaceMapGL3x::OnTimer(UINT_PTR nIDEvent)
{
    if (nIDEvent == m_tmrId)
    {
        m_pProj->Update();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_program.SetMVP( m_pProj->GetCombinedMVPMatrix() );
        m_program.SetGeoCenter( static_pointer_cast<ProjectorGIS>(m_pProj)->GetGeoCenter() );

        RenderGrid();

        m_program.SetDirectionalLight(m_lightShapes);

        map<GLuint, vector<GeoPoint>>::const_iterator it = m_shapes.begin();
        for (; it != m_shapes.end(); ++it)
        {
            glBindBuffer(GL_ARRAY_BUFFER, it->first);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GeoPoint), 0);

            glDrawArrays(GL_LINE_STRIP, 0, it->second.size());  // first and last points are the same in the data
        }

        m_pGLCtx->SwapBuffers();
    }

    DrawingSurface::OnTimer(nIDEvent);
}

void DrawingSurfaceMapGL3x::AddPolygon(const vector<GeoPoint>& veGeoPts)
{
    if ( veGeoPts.size() > 1 )  // at least two points
    {
        GLuint buffId;
        glGenBuffers(1, &buffId);
        glBindBuffer(GL_ARRAY_BUFFER, buffId);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GeoPoint) * veGeoPts.size(), &veGeoPts[0], GL_STATIC_DRAW);

        m_shapes.insert(pair<GLuint, vector<GeoPoint>>(buffId, veGeoPts));
    }
}

void DrawingSurfaceMapGL3x::RemovePolygons()
{
    map<GLuint, vector<GeoPoint>>::const_iterator it = m_shapes.begin();
    for (; it != m_shapes.end(); ++it)
    {
        glDeleteBuffers(1, &(it->first));
    }
    m_shapes.clear();
}

void DrawingSurfaceMapGL3x::RenderGrid()
{
    glBindBuffer(GL_ARRAY_BUFFER, m_gridId);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

    // draw the axes through origin with a different color
    m_program.SetDirectionalLight(m_lightGridAxes);
    glDrawArrays(GL_LINES, 0, 4);

    // draw the grid
    m_program.SetDirectionalLight(m_lightGrid);
    glDrawArrays(GL_LINES, 4, 2 * (4 * Grid::HALF_SIDE + 2) - 4);
}

void DrawingSurfaceMapGL3x::OnMouseMove(UINT nFlags, CPoint point)
{
    CWnd* pParent = GetParent();
    if (pParent)
    {
        GeoPoint out;
        VERIFY( m_pProj->UnprojectPoint( ProjPoint(point.x, point.y), out) );
        static_cast<CProjectionsDlg*>(pParent)->UpdateStatusBar( out.x, out.y );
        SetFocus();
    }
}
