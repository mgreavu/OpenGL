#pragma once
#include "Resource.h"
#include "afxwin.h"
#include "common/BoundingBox.h"
#include <vector>
#include <memory>

using std::vector;
using std::shared_ptr;

class DrawingSurfaceMapGL3x;
class GeoMap;
class ProjectorGIS;

class CProjectionsDlg : public CDialogEx
{
public:
    CProjectionsDlg(CWnd* pParent = NULL);
    ~CProjectionsDlg();

    enum { IDD = IDD_PROJECTIONS_DIALOG };

    BOOL PreTranslateMessage(MSG* pMsg);
    void UpdateStatusBar(float lon, float lat);

protected:
    bool InitDS();

    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnFileOpengl3x();
    afx_msg void OnFileExit();
    afx_msg void OnToolsCamera();
    afx_msg void OnToolsLocation();
    afx_msg void OnHelpAbout();
    DECLARE_MESSAGE_MAP()

    bool LoadMaps();

    shared_ptr<DrawingSurfaceMapGL3x> m_pDrawingSurface;
    shared_ptr<ProjectorGIS>  m_pProj;

    vector<shared_ptr<GeoMap>> m_veMaps;
    BoundingBox m_geoArea;

    HICON m_hIcon;
    CMenu m_menu;
    CStatusBarCtrl m_statusBar;
    int            m_SBarWidths[2];
};
