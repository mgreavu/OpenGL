#include "stdafx.h"
#include "Projections.h"
#include "ProjectionParamsDlg.h"

IMPLEMENT_DYNAMIC(ProjectionParamsDlg, CDialog)

ProjectionParamsDlg::ProjectionParamsDlg(CWnd* pParent, float res)
	: CDialog(ProjectionParamsDlg::IDD, pParent)
    , m_Res(res)
{
}

ProjectionParamsDlg::~ProjectionParamsDlg()
{
}

void ProjectionParamsDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_RESOLUTION, m_Res);
	DDV_MinMaxFloat(pDX, m_Res, 0.1f, 131072);
}


BEGIN_MESSAGE_MAP(ProjectionParamsDlg, CDialog)
    ON_BN_CLICKED(IDOK, &ProjectionParamsDlg::OnBnClickedOk)
END_MESSAGE_MAP()


BOOL ProjectionParamsDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    UpdateData(FALSE);

    return TRUE;
}

void ProjectionParamsDlg::OnBnClickedOk()
{
    UpdateData();

    OnOK();
}

