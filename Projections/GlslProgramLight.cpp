#include "stdafx.h"
#include "GlslProgramLight.h"
#include <glm/gtc/type_ptr.hpp>

GlslProgramLight::GlslProgramLight()
{
}

bool GlslProgramLight::Init()
{
    if (!GlslProgram::Init())
    {
        return false;
    }

    if (!AddShader(GL_VERTEX_SHADER, "projections.vs"))
    {
        return false;
    }

    if (!AddShader(GL_FRAGMENT_SHADER, "projections.fs"))
    {
        return false;
    }

    if (!Finalize())
    {
        return false;
    }

    m_MVPLocation = GetUniformLocation("gMVP");
    m_GeoCenter = GetUniformLocation("gGeoCenter");
    m_dirLightColorLocation = GetUniformLocation("gDirectionalLight.Color");
    m_dirLightAmbientIntensityLocation = GetUniformLocation("gDirectionalLight.AmbientIntensity");

    if (m_MVPLocation == 0xFFFFFFFF ||
        m_GeoCenter == 0xFFFFFFFF ||
        m_dirLightAmbientIntensityLocation == 0xFFFFFFFF ||
        m_dirLightColorLocation == 0xFFFFFFFF)
    {
        return false;
    }

    return true;
}

void GlslProgramLight::SetMVP(const glm::mat4& MVP)
{
    glUniformMatrix4fv(m_MVPLocation, 1, GL_FALSE, glm::value_ptr(MVP));
}

void GlslProgramLight::SetGeoCenter( const glm::vec3& center )
{
    glUniform3f(m_GeoCenter, center.x, center.y, center.z);
}

void GlslProgramLight::SetDirectionalLight(const DirectionalLight& Light)
{
    glUniform3f(m_dirLightColorLocation, Light.m_color.x, Light.m_color.y, Light.m_color.z);
    glUniform1f(m_dirLightAmbientIntensityLocation, Light.m_ambientIntensity);
}
