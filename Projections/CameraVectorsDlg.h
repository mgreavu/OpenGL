#pragma once
#include <glm/vec3.hpp>

class CameraVectorsDlg : public CDialogEx
{
    DECLARE_DYNAMIC(CameraVectorsDlg)

public:
    CameraVectorsDlg(CWnd* pParent, const glm::vec3& pos, const glm::vec3& lookAt, const glm::vec3& up);
    virtual ~CameraVectorsDlg();

    enum { IDD = IDD_CAM_VECTORS };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    void DDX_Text_Formatted(CDataExchange* pDX,
        int nIDC,
        float& value,
        LPCTSTR lpszOutFormat=_T("%lf"),
        LPCTSTR lpszInFormat=_T("%f"));

    afx_msg void OnBnClickedOk();
    virtual BOOL OnInitDialog();

    DECLARE_MESSAGE_MAP()

public:
    float m_PosX;
    float m_PosY;
    float m_PosZ;
    float m_AtX;
    float m_AtY;
    float m_AtZ;
    float m_UpX;
    float m_UpY;
    float m_UpZ;
};
